/******/ (function(modules) { // webpackBootstrap
/******/ 	function hotDownloadUpdateChunk(chunkId) { // eslint-disable-line no-unused-vars
/******/ 		var chunk = require("./" + "" + chunkId + "." + hotCurrentHash + ".hot-update.js");
/******/ 		hotAddUpdateChunk(chunk.id, chunk.modules);
/******/ 	}
/******/ 	
/******/ 	function hotDownloadManifest(callback) { // eslint-disable-line no-unused-vars
/******/ 		try {
/******/ 			var update = require("./" + "" + hotCurrentHash + ".hot-update.json");
/******/ 		} catch(e) {
/******/ 			return callback();
/******/ 		}
/******/ 		callback(null, update);
/******/ 	}

/******/ 	
/******/ 	
/******/ 	// Copied from https://github.com/facebook/react/blob/bef45b0/src/shared/utils/canDefineProperty.js
/******/ 	var canDefineProperty = false;
/******/ 	try {
/******/ 		Object.defineProperty({}, "x", {
/******/ 			get: function() {}
/******/ 		});
/******/ 		canDefineProperty = true;
/******/ 	} catch(x) {
/******/ 		// IE will fail on defineProperty
/******/ 	}
/******/ 	
/******/ 	var hotApplyOnUpdate = true;
/******/ 	var hotCurrentHash = "a1bea905623132e1e699"; // eslint-disable-line no-unused-vars
/******/ 	var hotCurrentModuleData = {};
/******/ 	var hotCurrentParents = []; // eslint-disable-line no-unused-vars
/******/ 	
/******/ 	function hotCreateRequire(moduleId) { // eslint-disable-line no-unused-vars
/******/ 		var me = installedModules[moduleId];
/******/ 		if(!me) return __webpack_require__;
/******/ 		var fn = function(request) {
/******/ 			if(me.hot.active) {
/******/ 				if(installedModules[request]) {
/******/ 					if(installedModules[request].parents.indexOf(moduleId) < 0)
/******/ 						installedModules[request].parents.push(moduleId);
/******/ 					if(me.children.indexOf(request) < 0)
/******/ 						me.children.push(request);
/******/ 				} else hotCurrentParents = [moduleId];
/******/ 			} else {
/******/ 				console.warn("[HMR] unexpected require(" + request + ") from disposed module " + moduleId);
/******/ 				hotCurrentParents = [];
/******/ 			}
/******/ 			return __webpack_require__(request);
/******/ 		};
/******/ 		for(var name in __webpack_require__) {
/******/ 			if(Object.prototype.hasOwnProperty.call(__webpack_require__, name)) {
/******/ 				if(canDefineProperty) {
/******/ 					Object.defineProperty(fn, name, (function(name) {
/******/ 						return {
/******/ 							configurable: true,
/******/ 							enumerable: true,
/******/ 							get: function() {
/******/ 								return __webpack_require__[name];
/******/ 							},
/******/ 							set: function(value) {
/******/ 								__webpack_require__[name] = value;
/******/ 							}
/******/ 						};
/******/ 					}(name)));
/******/ 				} else {
/******/ 					fn[name] = __webpack_require__[name];
/******/ 				}
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		function ensure(chunkId, callback) {
/******/ 			if(hotStatus === "ready")
/******/ 				hotSetStatus("prepare");
/******/ 			hotChunksLoading++;
/******/ 			__webpack_require__.e(chunkId, function() {
/******/ 				try {
/******/ 					callback.call(null, fn);
/******/ 				} finally {
/******/ 					finishChunkLoading();
/******/ 				}
/******/ 	
/******/ 				function finishChunkLoading() {
/******/ 					hotChunksLoading--;
/******/ 					if(hotStatus === "prepare") {
/******/ 						if(!hotWaitingFilesMap[chunkId]) {
/******/ 							hotEnsureUpdateChunk(chunkId);
/******/ 						}
/******/ 						if(hotChunksLoading === 0 && hotWaitingFiles === 0) {
/******/ 							hotUpdateDownloaded();
/******/ 						}
/******/ 					}
/******/ 				}
/******/ 			});
/******/ 		}
/******/ 		if(canDefineProperty) {
/******/ 			Object.defineProperty(fn, "e", {
/******/ 				enumerable: true,
/******/ 				value: ensure
/******/ 			});
/******/ 		} else {
/******/ 			fn.e = ensure;
/******/ 		}
/******/ 		return fn;
/******/ 	}
/******/ 	
/******/ 	function hotCreateModule(moduleId) { // eslint-disable-line no-unused-vars
/******/ 		var hot = {
/******/ 			// private stuff
/******/ 			_acceptedDependencies: {},
/******/ 			_declinedDependencies: {},
/******/ 			_selfAccepted: false,
/******/ 			_selfDeclined: false,
/******/ 			_disposeHandlers: [],
/******/ 	
/******/ 			// Module API
/******/ 			active: true,
/******/ 			accept: function(dep, callback) {
/******/ 				if(typeof dep === "undefined")
/******/ 					hot._selfAccepted = true;
/******/ 				else if(typeof dep === "function")
/******/ 					hot._selfAccepted = dep;
/******/ 				else if(typeof dep === "object")
/******/ 					for(var i = 0; i < dep.length; i++)
/******/ 						hot._acceptedDependencies[dep[i]] = callback;
/******/ 				else
/******/ 					hot._acceptedDependencies[dep] = callback;
/******/ 			},
/******/ 			decline: function(dep) {
/******/ 				if(typeof dep === "undefined")
/******/ 					hot._selfDeclined = true;
/******/ 				else if(typeof dep === "number")
/******/ 					hot._declinedDependencies[dep] = true;
/******/ 				else
/******/ 					for(var i = 0; i < dep.length; i++)
/******/ 						hot._declinedDependencies[dep[i]] = true;
/******/ 			},
/******/ 			dispose: function(callback) {
/******/ 				hot._disposeHandlers.push(callback);
/******/ 			},
/******/ 			addDisposeHandler: function(callback) {
/******/ 				hot._disposeHandlers.push(callback);
/******/ 			},
/******/ 			removeDisposeHandler: function(callback) {
/******/ 				var idx = hot._disposeHandlers.indexOf(callback);
/******/ 				if(idx >= 0) hot._disposeHandlers.splice(idx, 1);
/******/ 			},
/******/ 	
/******/ 			// Management API
/******/ 			check: hotCheck,
/******/ 			apply: hotApply,
/******/ 			status: function(l) {
/******/ 				if(!l) return hotStatus;
/******/ 				hotStatusHandlers.push(l);
/******/ 			},
/******/ 			addStatusHandler: function(l) {
/******/ 				hotStatusHandlers.push(l);
/******/ 			},
/******/ 			removeStatusHandler: function(l) {
/******/ 				var idx = hotStatusHandlers.indexOf(l);
/******/ 				if(idx >= 0) hotStatusHandlers.splice(idx, 1);
/******/ 			},
/******/ 	
/******/ 			//inherit from previous dispose call
/******/ 			data: hotCurrentModuleData[moduleId]
/******/ 		};
/******/ 		return hot;
/******/ 	}
/******/ 	
/******/ 	var hotStatusHandlers = [];
/******/ 	var hotStatus = "idle";
/******/ 	
/******/ 	function hotSetStatus(newStatus) {
/******/ 		hotStatus = newStatus;
/******/ 		for(var i = 0; i < hotStatusHandlers.length; i++)
/******/ 			hotStatusHandlers[i].call(null, newStatus);
/******/ 	}
/******/ 	
/******/ 	// while downloading
/******/ 	var hotWaitingFiles = 0;
/******/ 	var hotChunksLoading = 0;
/******/ 	var hotWaitingFilesMap = {};
/******/ 	var hotRequestedFilesMap = {};
/******/ 	var hotAvailibleFilesMap = {};
/******/ 	var hotCallback;
/******/ 	
/******/ 	// The update info
/******/ 	var hotUpdate, hotUpdateNewHash;
/******/ 	
/******/ 	function toModuleId(id) {
/******/ 		var isNumber = (+id) + "" === id;
/******/ 		return isNumber ? +id : id;
/******/ 	}
/******/ 	
/******/ 	function hotCheck(apply, callback) {
/******/ 		if(hotStatus !== "idle") throw new Error("check() is only allowed in idle status");
/******/ 		if(typeof apply === "function") {
/******/ 			hotApplyOnUpdate = false;
/******/ 			callback = apply;
/******/ 		} else {
/******/ 			hotApplyOnUpdate = apply;
/******/ 			callback = callback || function(err) {
/******/ 				if(err) throw err;
/******/ 			};
/******/ 		}
/******/ 		hotSetStatus("check");
/******/ 		hotDownloadManifest(function(err, update) {
/******/ 			if(err) return callback(err);
/******/ 			if(!update) {
/******/ 				hotSetStatus("idle");
/******/ 				callback(null, null);
/******/ 				return;
/******/ 			}
/******/ 	
/******/ 			hotRequestedFilesMap = {};
/******/ 			hotAvailibleFilesMap = {};
/******/ 			hotWaitingFilesMap = {};
/******/ 			for(var i = 0; i < update.c.length; i++)
/******/ 				hotAvailibleFilesMap[update.c[i]] = true;
/******/ 			hotUpdateNewHash = update.h;
/******/ 	
/******/ 			hotSetStatus("prepare");
/******/ 			hotCallback = callback;
/******/ 			hotUpdate = {};
/******/ 			var chunkId = 0;
/******/ 			{ // eslint-disable-line no-lone-blocks
/******/ 				/*globals chunkId */
/******/ 				hotEnsureUpdateChunk(chunkId);
/******/ 			}
/******/ 			if(hotStatus === "prepare" && hotChunksLoading === 0 && hotWaitingFiles === 0) {
/******/ 				hotUpdateDownloaded();
/******/ 			}
/******/ 		});
/******/ 	}
/******/ 	
/******/ 	function hotAddUpdateChunk(chunkId, moreModules) { // eslint-disable-line no-unused-vars
/******/ 		if(!hotAvailibleFilesMap[chunkId] || !hotRequestedFilesMap[chunkId])
/******/ 			return;
/******/ 		hotRequestedFilesMap[chunkId] = false;
/******/ 		for(var moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				hotUpdate[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(--hotWaitingFiles === 0 && hotChunksLoading === 0) {
/******/ 			hotUpdateDownloaded();
/******/ 		}
/******/ 	}
/******/ 	
/******/ 	function hotEnsureUpdateChunk(chunkId) {
/******/ 		if(!hotAvailibleFilesMap[chunkId]) {
/******/ 			hotWaitingFilesMap[chunkId] = true;
/******/ 		} else {
/******/ 			hotRequestedFilesMap[chunkId] = true;
/******/ 			hotWaitingFiles++;
/******/ 			hotDownloadUpdateChunk(chunkId);
/******/ 		}
/******/ 	}
/******/ 	
/******/ 	function hotUpdateDownloaded() {
/******/ 		hotSetStatus("ready");
/******/ 		var callback = hotCallback;
/******/ 		hotCallback = null;
/******/ 		if(!callback) return;
/******/ 		if(hotApplyOnUpdate) {
/******/ 			hotApply(hotApplyOnUpdate, callback);
/******/ 		} else {
/******/ 			var outdatedModules = [];
/******/ 			for(var id in hotUpdate) {
/******/ 				if(Object.prototype.hasOwnProperty.call(hotUpdate, id)) {
/******/ 					outdatedModules.push(toModuleId(id));
/******/ 				}
/******/ 			}
/******/ 			callback(null, outdatedModules);
/******/ 		}
/******/ 	}
/******/ 	
/******/ 	function hotApply(options, callback) {
/******/ 		if(hotStatus !== "ready") throw new Error("apply() is only allowed in ready status");
/******/ 		if(typeof options === "function") {
/******/ 			callback = options;
/******/ 			options = {};
/******/ 		} else if(options && typeof options === "object") {
/******/ 			callback = callback || function(err) {
/******/ 				if(err) throw err;
/******/ 			};
/******/ 		} else {
/******/ 			options = {};
/******/ 			callback = callback || function(err) {
/******/ 				if(err) throw err;
/******/ 			};
/******/ 		}
/******/ 	
/******/ 		function getAffectedStuff(module) {
/******/ 			var outdatedModules = [module];
/******/ 			var outdatedDependencies = {};
/******/ 	
/******/ 			var queue = outdatedModules.slice();
/******/ 			while(queue.length > 0) {
/******/ 				var moduleId = queue.pop();
/******/ 				var module = installedModules[moduleId];
/******/ 				if(!module || module.hot._selfAccepted)
/******/ 					continue;
/******/ 				if(module.hot._selfDeclined) {
/******/ 					return new Error("Aborted because of self decline: " + moduleId);
/******/ 				}
/******/ 				if(moduleId === 0) {
/******/ 					return;
/******/ 				}
/******/ 				for(var i = 0; i < module.parents.length; i++) {
/******/ 					var parentId = module.parents[i];
/******/ 					var parent = installedModules[parentId];
/******/ 					if(parent.hot._declinedDependencies[moduleId]) {
/******/ 						return new Error("Aborted because of declined dependency: " + moduleId + " in " + parentId);
/******/ 					}
/******/ 					if(outdatedModules.indexOf(parentId) >= 0) continue;
/******/ 					if(parent.hot._acceptedDependencies[moduleId]) {
/******/ 						if(!outdatedDependencies[parentId])
/******/ 							outdatedDependencies[parentId] = [];
/******/ 						addAllToSet(outdatedDependencies[parentId], [moduleId]);
/******/ 						continue;
/******/ 					}
/******/ 					delete outdatedDependencies[parentId];
/******/ 					outdatedModules.push(parentId);
/******/ 					queue.push(parentId);
/******/ 				}
/******/ 			}
/******/ 	
/******/ 			return [outdatedModules, outdatedDependencies];
/******/ 		}
/******/ 	
/******/ 		function addAllToSet(a, b) {
/******/ 			for(var i = 0; i < b.length; i++) {
/******/ 				var item = b[i];
/******/ 				if(a.indexOf(item) < 0)
/******/ 					a.push(item);
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// at begin all updates modules are outdated
/******/ 		// the "outdated" status can propagate to parents if they don't accept the children
/******/ 		var outdatedDependencies = {};
/******/ 		var outdatedModules = [];
/******/ 		var appliedUpdate = {};
/******/ 		for(var id in hotUpdate) {
/******/ 			if(Object.prototype.hasOwnProperty.call(hotUpdate, id)) {
/******/ 				var moduleId = toModuleId(id);
/******/ 				var result = getAffectedStuff(moduleId);
/******/ 				if(!result) {
/******/ 					if(options.ignoreUnaccepted)
/******/ 						continue;
/******/ 					hotSetStatus("abort");
/******/ 					return callback(new Error("Aborted because " + moduleId + " is not accepted"));
/******/ 				}
/******/ 				if(result instanceof Error) {
/******/ 					hotSetStatus("abort");
/******/ 					return callback(result);
/******/ 				}
/******/ 				appliedUpdate[moduleId] = hotUpdate[moduleId];
/******/ 				addAllToSet(outdatedModules, result[0]);
/******/ 				for(var moduleId in result[1]) {
/******/ 					if(Object.prototype.hasOwnProperty.call(result[1], moduleId)) {
/******/ 						if(!outdatedDependencies[moduleId])
/******/ 							outdatedDependencies[moduleId] = [];
/******/ 						addAllToSet(outdatedDependencies[moduleId], result[1][moduleId]);
/******/ 					}
/******/ 				}
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// Store self accepted outdated modules to require them later by the module system
/******/ 		var outdatedSelfAcceptedModules = [];
/******/ 		for(var i = 0; i < outdatedModules.length; i++) {
/******/ 			var moduleId = outdatedModules[i];
/******/ 			if(installedModules[moduleId] && installedModules[moduleId].hot._selfAccepted)
/******/ 				outdatedSelfAcceptedModules.push({
/******/ 					module: moduleId,
/******/ 					errorHandler: installedModules[moduleId].hot._selfAccepted
/******/ 				});
/******/ 		}
/******/ 	
/******/ 		// Now in "dispose" phase
/******/ 		hotSetStatus("dispose");
/******/ 		var queue = outdatedModules.slice();
/******/ 		while(queue.length > 0) {
/******/ 			var moduleId = queue.pop();
/******/ 			var module = installedModules[moduleId];
/******/ 			if(!module) continue;
/******/ 	
/******/ 			var data = {};
/******/ 	
/******/ 			// Call dispose handlers
/******/ 			var disposeHandlers = module.hot._disposeHandlers;
/******/ 			for(var j = 0; j < disposeHandlers.length; j++) {
/******/ 				var cb = disposeHandlers[j];
/******/ 				cb(data);
/******/ 			}
/******/ 			hotCurrentModuleData[moduleId] = data;
/******/ 	
/******/ 			// disable module (this disables requires from this module)
/******/ 			module.hot.active = false;
/******/ 	
/******/ 			// remove module from cache
/******/ 			delete installedModules[moduleId];
/******/ 	
/******/ 			// remove "parents" references from all children
/******/ 			for(var j = 0; j < module.children.length; j++) {
/******/ 				var child = installedModules[module.children[j]];
/******/ 				if(!child) continue;
/******/ 				var idx = child.parents.indexOf(moduleId);
/******/ 				if(idx >= 0) {
/******/ 					child.parents.splice(idx, 1);
/******/ 				}
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// remove outdated dependency from module children
/******/ 		for(var moduleId in outdatedDependencies) {
/******/ 			if(Object.prototype.hasOwnProperty.call(outdatedDependencies, moduleId)) {
/******/ 				var module = installedModules[moduleId];
/******/ 				var moduleOutdatedDependencies = outdatedDependencies[moduleId];
/******/ 				for(var j = 0; j < moduleOutdatedDependencies.length; j++) {
/******/ 					var dependency = moduleOutdatedDependencies[j];
/******/ 					var idx = module.children.indexOf(dependency);
/******/ 					if(idx >= 0) module.children.splice(idx, 1);
/******/ 				}
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// Not in "apply" phase
/******/ 		hotSetStatus("apply");
/******/ 	
/******/ 		hotCurrentHash = hotUpdateNewHash;
/******/ 	
/******/ 		// insert new code
/******/ 		for(var moduleId in appliedUpdate) {
/******/ 			if(Object.prototype.hasOwnProperty.call(appliedUpdate, moduleId)) {
/******/ 				modules[moduleId] = appliedUpdate[moduleId];
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// call accept handlers
/******/ 		var error = null;
/******/ 		for(var moduleId in outdatedDependencies) {
/******/ 			if(Object.prototype.hasOwnProperty.call(outdatedDependencies, moduleId)) {
/******/ 				var module = installedModules[moduleId];
/******/ 				var moduleOutdatedDependencies = outdatedDependencies[moduleId];
/******/ 				var callbacks = [];
/******/ 				for(var i = 0; i < moduleOutdatedDependencies.length; i++) {
/******/ 					var dependency = moduleOutdatedDependencies[i];
/******/ 					var cb = module.hot._acceptedDependencies[dependency];
/******/ 					if(callbacks.indexOf(cb) >= 0) continue;
/******/ 					callbacks.push(cb);
/******/ 				}
/******/ 				for(var i = 0; i < callbacks.length; i++) {
/******/ 					var cb = callbacks[i];
/******/ 					try {
/******/ 						cb(outdatedDependencies);
/******/ 					} catch(err) {
/******/ 						if(!error)
/******/ 							error = err;
/******/ 					}
/******/ 				}
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// Load self accepted modules
/******/ 		for(var i = 0; i < outdatedSelfAcceptedModules.length; i++) {
/******/ 			var item = outdatedSelfAcceptedModules[i];
/******/ 			var moduleId = item.module;
/******/ 			hotCurrentParents = [moduleId];
/******/ 			try {
/******/ 				__webpack_require__(moduleId);
/******/ 			} catch(err) {
/******/ 				if(typeof item.errorHandler === "function") {
/******/ 					try {
/******/ 						item.errorHandler(err);
/******/ 					} catch(err) {
/******/ 						if(!error)
/******/ 							error = err;
/******/ 					}
/******/ 				} else if(!error)
/******/ 					error = err;
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// handle errors in accept handlers and self accepted module load
/******/ 		if(error) {
/******/ 			hotSetStatus("fail");
/******/ 			return callback(error);
/******/ 		}
/******/ 	
/******/ 		hotSetStatus("idle");
/******/ 		callback(null, outdatedModules);
/******/ 	}

/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false,
/******/ 			hot: hotCreateModule(moduleId),
/******/ 			parents: hotCurrentParents,
/******/ 			children: []
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, hotCreateRequire(moduleId));

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "http://localhost:8080/built/";

/******/ 	// __webpack_hash__
/******/ 	__webpack_require__.h = function() { return hotCurrentHash; };

/******/ 	// Load entry module and return exports
/******/ 	return hotCreateRequire(0)(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(1);
	module.exports = __webpack_require__(3);


/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	/*globals window __webpack_hash__ */
	if(true) {
		var lastData;
		var upToDate = function upToDate() {
			return lastData.indexOf(__webpack_require__.h()) >= 0;
		};
		var check = function check() {
			module.hot.check(true, function(err, updatedModules) {
				if(err) {
					if(module.hot.status() in {
							abort: 1,
							fail: 1
						}) {
						console.warn("[HMR] Cannot apply update. Need to do a full reload!");
						console.warn("[HMR] " + err.stack || err.message);
						window.location.reload();
					} else {
						console.warn("[HMR] Update failed: " + err.stack || err.message);
					}
					return;
				}

				if(!updatedModules) {
					console.warn("[HMR] Cannot find update. Need to do a full reload!");
					console.warn("[HMR] (Probably because of restarting the webpack-dev-server)");
					window.location.reload();
					return;
				}

				if(!upToDate()) {
					check();
				}

				__webpack_require__(2)(updatedModules, updatedModules);

				if(upToDate()) {
					console.log("[HMR] App is up to date.");
				}

			});
		};
		var addEventListener = window.addEventListener ? function(eventName, listener) {
			window.addEventListener(eventName, listener, false);
		} : function(eventName, listener) {
			window.attachEvent("on" + eventName, listener);
		};
		addEventListener("message", function(event) {
			if(typeof event.data === "string" && event.data.indexOf("webpackHotUpdate") === 0) {
				lastData = event.data;
				if(!upToDate() && module.hot.status() === "idle") {
					console.log("[HMR] Checking for updates on the server...");
					check();
				}
			}
		});
		console.log("[HMR] Waiting for update signal from WDS...");
	} else {
		throw new Error("[HMR] Hot Module Replacement is disabled.");
	}


/***/ },
/* 2 */
/***/ function(module, exports) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	module.exports = function(updatedModules, renewedModules) {
		var unacceptedModules = updatedModules.filter(function(moduleId) {
			return renewedModules && renewedModules.indexOf(moduleId) < 0;
		});

		if(unacceptedModules.length > 0) {
			console.warn("[HMR] The following modules couldn't be hot updated: (They would need a full reload!)");
			unacceptedModules.forEach(function(moduleId) {
				console.warn("[HMR]  - " + moduleId);
			});
		}

		if(!renewedModules || renewedModules.length === 0) {
			console.log("[HMR] Nothing hot updated.");
		} else {
			console.log("[HMR] Updated modules:");
			renewedModules.forEach(function(moduleId) {
				console.log("[HMR]  - " + moduleId);
			});
		}
	};


/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _react = __webpack_require__(4);

	var _react2 = _interopRequireDefault(_react);

	var _reactDom = __webpack_require__(5);

	var _reactDom2 = _interopRequireDefault(_reactDom);

	var _index = __webpack_require__(6);

	var _index2 = _interopRequireDefault(_index);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	_reactDom2.default.render(_react2.default.createElement(_index2.default, null), document.getElementById('content'));

/***/ },
/* 4 */
/***/ function(module, exports) {

	module.exports = require("react");

/***/ },
/* 5 */
/***/ function(module, exports) {

	module.exports = require("react-dom");

/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _react = __webpack_require__(4);

	var _react2 = _interopRequireDefault(_react);

	var _axios = __webpack_require__(7);

	var _axios2 = _interopRequireDefault(_axios);

	var _config = __webpack_require__(8);

	var _wrapper = __webpack_require__(10);

	var _wrapper2 = _interopRequireDefault(_wrapper);

	var _login = __webpack_require__(47);

	var _login2 = _interopRequireDefault(_login);

	var _index = __webpack_require__(50);

	var _index2 = _interopRequireDefault(_index);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	// This file to handle routing via state
	var Index = _react2.default.createClass({
	  displayName: 'Index',
	  getInitialState: function getInitialState() {
	    return {
	      loggedIn: false,
	      username: 'admin'
	    };
	  },
	  login: function login(username) {
	    this.setState({
	      loggedIn: true,
	      username: username
	    });
	  },
	  logout: function logout() {
	    this.setState({
	      loggedIn: false,
	      username: ''
	    });
	  },
	  render: function render() {
	    if (this.state.loggedIn) {
	      return _react2.default.createElement(_wrapper2.default, { logout: this.logout, username: this.state.username });
	    }
	    return _react2.default.createElement(_login2.default, { login: this.login });
	  }
	});

	module.exports = Index;

/***/ },
/* 7 */
/***/ function(module, exports) {

	module.exports = require("axios");

/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _platform = __webpack_require__(9);

	var _platform2 = _interopRequireDefault(_platform);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	module.exports = {

	  ///////////////////
	  //  DEVELOPMENT  //
	  ///////////////////

	  // server: 'http://localhost:3030',
	  // WSServer: 'ws://localhost:3030',
	  // runGameClient: {
	  //   'linux': './executables/LinuxClient.x86_64',
	  //   'win32': 'executables\\client.exe',
	  // }[platform.os.family.toLowerCase()],


	  //////////////////
	  //  PRODUCTION  //
	  //////////////////

	  server: 'http://138.68.53.198:3030',
	  WSServer: 'ws://138.68.53.198:3030',
	  // server: 'http://192.168.10.140:3030',
	  // WSServer: 'ws://192.168.10.140:3030',
	  runGameClient: {
	    'linux': './resources/app/executables/LinuxClient.x86_64',
	    'win32': 'resources\\app\\executables\\client.exe'
	  }[_platform2.default.os.family.toLowerCase()]

	};

/***/ },
/* 9 */
/***/ function(module, exports) {

	module.exports = require("platform");

/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _react = __webpack_require__(4);

	var _react2 = _interopRequireDefault(_react);

	var _play = __webpack_require__(11);

	var _play2 = _interopRequireDefault(_play);

	var _queue = __webpack_require__(31);

	var _queue2 = _interopRequireDefault(_queue);

	var _selection = __webpack_require__(38);

	var _selection2 = _interopRequireDefault(_selection);

	var _learn = __webpack_require__(44);

	var _learn2 = _interopRequireDefault(_learn);

	var _wrapper = __webpack_require__(45);

	var _wrapper2 = _interopRequireDefault(_wrapper);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	// This file to handle routing via state
	var Wrapper = _react2.default.createClass({
	  displayName: 'Wrapper',
	  getInitialState: function getInitialState() {
	    return {
	      page: 'Play',
	      playPage: 'Play',
	      game: {}
	    };
	  },
	  goToPage: function goToPage(page) {
	    // don't let people leave the selection page except to return to play after game
	    if (page !== 'Play' && this.state.page === 'Selection') {
	      return;
	    }
	    var playPages = ['Play', 'Queue', 'Selection'];
	    var nextPage = playPages.includes(page) ? page : this.state.playPage;
	    this.setState({
	      page: page,
	      playPage: nextPage
	    });
	  },
	  enterQueue: function enterQueue(game) {
	    this.setState({
	      page: 'Queue',
	      playPage: 'Queue',
	      game: game
	    });
	  },
	  leaveQueue: function leaveQueue() {
	    this.setState({
	      page: 'Play',
	      playPage: 'Queue',
	      game: {}
	    });
	  },
	  attemptLogout: function attemptLogout() {
	    // don't let people logout during selection
	    if (this.state.page !== 'Selection') {
	      this.props.logout();
	    }
	  },
	  renderNavLinks: function renderNavLinks() {
	    var _this = this;

	    var pages = [this.state.playPage, 'Learn'];
	    var notSelectedStyle = this.state.playPage === 'Selection' ? _wrapper2.default.disabledPage : _wrapper2.default.notSelectedPage;
	    return pages.map(function (page) {
	      var style = _this.state.page === page ? _wrapper2.default.selectedPage : notSelectedStyle;
	      return _react2.default.createElement(
	        'li',
	        { key: page, className: style, onClick: function onClick() {
	            return _this.goToPage(page);
	          } },
	        page
	      );
	    });
	  },
	  renderPage: function renderPage() {
	    var _this2 = this;

	    return {
	      'Play': _react2.default.createElement(_play2.default, {
	        username: this.props.username,
	        enterQueue: this.enterQueue }),
	      'Queue': _react2.default.createElement(_queue2.default, {
	        username: this.props.username,
	        leaveQueue: this.leaveQueue,
	        game: this.state.game,
	        goToSelection: function goToSelection() {
	          return _this2.goToPage('Selection');
	        } }),
	      'Selection': _react2.default.createElement(_selection2.default, {
	        username: this.props.username,
	        game: this.state.game,
	        goToPlay: function goToPlay() {
	          return _this2.goToPage('Play');
	        } }),
	      'Learn': _react2.default.createElement(_learn2.default, null)
	    }[this.state.page];
	  },
	  render: function render() {
	    var notSelectedStyle = this.state.playPage === 'Selection' ? _wrapper2.default.disabledPage : _wrapper2.default.notSelectedPage;
	    return _react2.default.createElement(
	      'div',
	      null,
	      _react2.default.createElement(
	        'ul',
	        null,
	        this.renderNavLinks(),
	        _react2.default.createElement(
	          'li',
	          { onClick: this.attemptLogout, className: notSelectedStyle },
	          'Logout'
	        )
	      ),
	      this.renderPage()
	    );
	  }
	});

	module.exports = Wrapper;

/***/ },
/* 11 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _react = __webpack_require__(4);

	var _react2 = _interopRequireDefault(_react);

	var _axios = __webpack_require__(7);

	var _axios2 = _interopRequireDefault(_axios);

	var _config = __webpack_require__(8);

	var _custom = __webpack_require__(12);

	var _custom2 = _interopRequireDefault(_custom);

	var _normal = __webpack_require__(26);

	var _normal2 = _interopRequireDefault(_normal);

	var _play = __webpack_require__(29);

	var _play2 = _interopRequireDefault(_play);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var Lobby = _react2.default.createClass({
	  displayName: 'Lobby',
	  getInitialState: function getInitialState() {
	    return {
	      gameType: 'Custom'
	    };
	  },
	  gameTypeHandler: function gameTypeHandler(type) {
	    this.setState({
	      gameType: type
	    });
	  },
	  renderGameType: function renderGameType() {
	    return {
	      'Normal': _react2.default.createElement(_normal2.default, { username: this.props.username }),
	      'Custom': _react2.default.createElement(_custom2.default, { username: this.props.username, enterQueue: this.props.enterQueue })
	    }[this.state.gameType];
	  },
	  renderGameTypeButtons: function renderGameTypeButtons() {
	    var _this = this;

	    var gameTypes = ['Normal', 'Custom'];
	    return gameTypes.map(function (type) {
	      var style = _this.state.gameType === type ? _play2.default.selectedBtn : _play2.default.notSelectedBtn;
	      return _react2.default.createElement(
	        'button',
	        { key: type, className: style, onClick: function onClick() {
	            return _this.gameTypeHandler(type);
	          } },
	        type
	      );
	    });
	  },
	  render: function render() {
	    return _react2.default.createElement(
	      'div',
	      { className: _play2.default.blackBox },
	      _react2.default.createElement(
	        'div',
	        { className: _play2.default.wrapper },
	        this.renderGameTypeButtons()
	      ),
	      this.renderGameType()
	    );
	  }
	});

	module.exports = Lobby;

/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _react = __webpack_require__(4);

	var _react2 = _interopRequireDefault(_react);

	var _create = __webpack_require__(13);

	var _create2 = _interopRequireDefault(_create);

	var _find = __webpack_require__(19);

	var _find2 = _interopRequireDefault(_find);

	var _custom = __webpack_require__(24);

	var _custom2 = _interopRequireDefault(_custom);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var Lobby = _react2.default.createClass({
	  displayName: 'Lobby',
	  getInitialState: function getInitialState() {
	    return {
	      customType: 'Find'
	    };
	  },
	  customTypeHandler: function customTypeHandler(type) {
	    this.setState({
	      customType: type
	    });
	  },
	  renderCustomType: function renderCustomType() {
	    return {
	      'Create': _react2.default.createElement(_create2.default, { username: this.props.username, enterQueue: this.props.enterQueue }),
	      'Find': _react2.default.createElement(_find2.default, { username: this.props.username, enterQueue: this.props.enterQueue })
	    }[this.state.customType];
	  },
	  renderCustomTypeButtons: function renderCustomTypeButtons() {
	    var _this = this;

	    var gameTypes = ['Create', 'Find'];
	    return gameTypes.map(function (type) {
	      var style = _this.state.customType === type ? _custom2.default.selectedBtn : _custom2.default.notSelectedBtn;
	      return _react2.default.createElement(
	        'button',
	        { key: type, className: style, onClick: function onClick() {
	            return _this.customTypeHandler(type);
	          } },
	        type
	      );
	    });
	  },
	  render: function render() {
	    return _react2.default.createElement(
	      'div',
	      null,
	      _react2.default.createElement(
	        'div',
	        { className: _custom2.default.wrapper },
	        this.renderCustomTypeButtons()
	      ),
	      this.renderCustomType()
	    );
	  }
	});

	module.exports = Lobby;

/***/ },
/* 13 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _react = __webpack_require__(4);

	var _react2 = _interopRequireDefault(_react);

	var _axios = __webpack_require__(7);

	var _axios2 = _interopRequireDefault(_axios);

	var _config = __webpack_require__(8);

	var _create = __webpack_require__(14);

	var _create2 = _interopRequireDefault(_create);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var Create = _react2.default.createClass({
	  displayName: 'Create',
	  getInitialState: function getInitialState() {
	    return {
	      numTeams: 4,
	      teamSize: 3,
	      feedbackText: ''
	    };
	  },
	  handleTeamSize: function handleTeamSize(size) {
	    this.setState({
	      teamSize: size,
	      numTeams: 2
	    });
	  },
	  handleNumTeams: function handleNumTeams(num) {
	    this.setState({
	      numTeams: num
	    });
	  },
	  createGameHandler: function createGameHandler() {
	    var self = this;
	    _axios2.default.post(_config.server + '/game/custom/create', {
	      creator: this.props.username,
	      gameName: this.refs.gameName.value.trim(),
	      gamePassword: this.refs.gamePassword.value,
	      numTeams: this.state.numTeams,
	      teamSize: this.state.teamSize
	    }).then(function (response) {
	      self.props.enterQueue(response.data);
	    }).catch(function (error) {
	      console.log(error);
	      self.setState({
	        feedbackText: error.response.data
	      });
	    });
	  },
	  renderTeamSizes: function renderTeamSizes() {
	    var _this = this;

	    var teamSizes = [1, 2, 3, 4, 5, 6];
	    return teamSizes.map(function (size) {
	      var btnStyle = _this.state.teamSize === size ? _create2.default.selectedCircle : _create2.default.notSelectedCircle;
	      return _react2.default.createElement(
	        'button',
	        { key: size, className: btnStyle, onClick: function onClick() {
	            return _this.handleTeamSize(size);
	          } },
	        size
	      );
	    });
	  },
	  renderNumTeams: function renderNumTeams() {
	    var _this2 = this;

	    var numTeams = {
	      1: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
	      2: [1, 2, 3, 4, 5, 6],
	      3: [1, 2, 3, 4],
	      4: [1, 2, 3],
	      5: [1, 2],
	      6: [1, 2]
	    }[this.state.teamSize];

	    return numTeams.map(function (num) {
	      var btnStyle = _this2.state.numTeams === num ? _create2.default.selectedCircle : _create2.default.notSelectedCircle;
	      return _react2.default.createElement(
	        'button',
	        { key: num, className: btnStyle, onClick: function onClick() {
	            return _this2.handleNumTeams(num);
	          } },
	        num
	      );
	    });
	  },
	  render: function render() {
	    return _react2.default.createElement(
	      'div',
	      null,
	      _react2.default.createElement(
	        'div',
	        null,
	        _react2.default.createElement('input', { ref: 'gameName', placeholder: 'Game Name' })
	      ),
	      _react2.default.createElement(
	        'div',
	        null,
	        _react2.default.createElement('input', { ref: 'gamePassword', placeholder: 'Password (optional)' })
	      ),
	      _react2.default.createElement(
	        'div',
	        null,
	        _react2.default.createElement(
	          'p',
	          { className: _create2.default.textLabel },
	          'Team Size:'
	        ),
	        this.renderTeamSizes()
	      ),
	      _react2.default.createElement(
	        'div',
	        null,
	        _react2.default.createElement(
	          'p',
	          { className: _create2.default.textLabel },
	          'Number of Teams:'
	        ),
	        this.renderNumTeams()
	      ),
	      _react2.default.createElement(
	        'p',
	        { className: _create2.default.feedback },
	        this.state.feedbackText
	      ),
	      _react2.default.createElement(
	        'button',
	        { className: _create2.default.submitBtn, onClick: this.createGameHandler },
	        'Create Game'
	      )
	    );
	  }
	});

	module.exports = Create;

/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(15);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(18)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(true) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept(15, function() {
				var newContent = __webpack_require__(15);
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 15 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(16)();
	// imports
	exports.i(__webpack_require__(17), undefined);

	// module
	exports.push([module.id, ".create__notSelectedCircle___1XqP7 {\r\n  padding: 5px;\r\n  margin-left: 5px;\r\n  height: 30px;\r\n  width: 30px;\r\n  background-color: " + __webpack_require__(17).locals["dark-blue"] + ";\r\n  color: white;\r\n  display: inline-block;\r\n  border-radius: 15px;\r\n}\r\n\r\n.create__selectedCircle___j5lpQ {\r\n  padding: 5px;\r\n  margin-left: 5px;\r\n  height: 30px;\r\n  width: 30px;\r\n  background-color: " + __webpack_require__(17).locals["blue"] + ";\r\n  color: white;\r\n  display: inline-block;\r\n  border-radius: 15px;\r\n}\r\n\r\n.create__textLabel___1d6Mb {\r\n  display: inline-block;\r\n  color: white;\r\n  font-size: 20px;\r\n}\r\n\r\n.create__feedback___1gpG5 {\r\n  font-size: 18px;\r\n  color: " + __webpack_require__(17).locals["red"] + ";\r\n  margin: 0;\r\n}\r\n\r\n.create__submitBtn___NO80B {\r\n  background-color: " + __webpack_require__(17).locals["green"] + ";\r\n  padding: 5px;\r\n  margin: 5px;\r\n  color: light-gray;\r\n  font-weight: bold;\r\n  font-size: 20px;\r\n}\r\n", ""]);

	// exports
	exports.locals = {
		"colors": "\"../../../../colors.css\"",
		"green": "" + __webpack_require__(17).locals["green"] + "",
		"red": "" + __webpack_require__(17).locals["red"] + "",
		"dark-blue": "" + __webpack_require__(17).locals["dark-blue"] + "",
		"blue": "" + __webpack_require__(17).locals["blue"] + "",
		"notSelectedCircle": "create__notSelectedCircle___1XqP7",
		"selectedCircle": "create__selectedCircle___j5lpQ",
		"textLabel": "create__textLabel___1d6Mb",
		"feedback": "create__feedback___1gpG5",
		"submitBtn": "create__submitBtn___NO80B"
	};

/***/ },
/* 16 */
/***/ function(module, exports) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	// css base code, injected by the css-loader
	module.exports = function() {
		var list = [];

		// return the list of modules as css string
		list.toString = function toString() {
			var result = [];
			for(var i = 0; i < this.length; i++) {
				var item = this[i];
				if(item[2]) {
					result.push("@media " + item[2] + "{" + item[1] + "}");
				} else {
					result.push(item[1]);
				}
			}
			return result.join("");
		};

		// import a list of modules into the list
		list.i = function(modules, mediaQuery) {
			if(typeof modules === "string")
				modules = [[null, modules, ""]];
			var alreadyImportedModules = {};
			for(var i = 0; i < this.length; i++) {
				var id = this[i][0];
				if(typeof id === "number")
					alreadyImportedModules[id] = true;
			}
			for(i = 0; i < modules.length; i++) {
				var item = modules[i];
				// skip already imported module
				// this implementation is not 100% perfect for weird media query combinations
				//  when a module is imported multiple times with different media queries.
				//  I hope this will never occur (Hey this way we have smaller bundles)
				if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
					if(mediaQuery && !item[2]) {
						item[2] = mediaQuery;
					} else if(mediaQuery) {
						item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
					}
					list.push(item);
				}
			}
		};
		return list;
	};


/***/ },
/* 17 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(16)();
	// imports


	// module
	exports.push([module.id, "/* COLORS */\r\n", ""]);

	// exports
	exports.locals = {
		"very-light-gray": "#c4c4c4",
		"light-gray": "#606060",
		"dark-gray": "#2a2d28",
		"green": "#92f28a",
		"red": "#ff4838",
		"lighter-blue": "#96beff",
		"light-blue": "#609dff",
		"blue": "#5c95f2",
		"dark-blue": "#003284"
	};

/***/ },
/* 18 */
/***/ function(module, exports, __webpack_require__) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	var stylesInDom = {},
		memoize = function(fn) {
			var memo;
			return function () {
				if (typeof memo === "undefined") memo = fn.apply(this, arguments);
				return memo;
			};
		},
		isOldIE = memoize(function() {
			return /msie [6-9]\b/.test(window.navigator.userAgent.toLowerCase());
		}),
		getHeadElement = memoize(function () {
			return document.head || document.getElementsByTagName("head")[0];
		}),
		singletonElement = null,
		singletonCounter = 0,
		styleElementsInsertedAtTop = [];

	module.exports = function(list, options) {
		if(false) {
			if(typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
		}

		options = options || {};
		// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
		// tags it will allow on a page
		if (typeof options.singleton === "undefined") options.singleton = isOldIE();

		// By default, add <style> tags to the bottom of <head>.
		if (typeof options.insertAt === "undefined") options.insertAt = "bottom";

		var styles = listToStyles(list);
		addStylesToDom(styles, options);

		return function update(newList) {
			var mayRemove = [];
			for(var i = 0; i < styles.length; i++) {
				var item = styles[i];
				var domStyle = stylesInDom[item.id];
				domStyle.refs--;
				mayRemove.push(domStyle);
			}
			if(newList) {
				var newStyles = listToStyles(newList);
				addStylesToDom(newStyles, options);
			}
			for(var i = 0; i < mayRemove.length; i++) {
				var domStyle = mayRemove[i];
				if(domStyle.refs === 0) {
					for(var j = 0; j < domStyle.parts.length; j++)
						domStyle.parts[j]();
					delete stylesInDom[domStyle.id];
				}
			}
		};
	}

	function addStylesToDom(styles, options) {
		for(var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];
			if(domStyle) {
				domStyle.refs++;
				for(var j = 0; j < domStyle.parts.length; j++) {
					domStyle.parts[j](item.parts[j]);
				}
				for(; j < item.parts.length; j++) {
					domStyle.parts.push(addStyle(item.parts[j], options));
				}
			} else {
				var parts = [];
				for(var j = 0; j < item.parts.length; j++) {
					parts.push(addStyle(item.parts[j], options));
				}
				stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
			}
		}
	}

	function listToStyles(list) {
		var styles = [];
		var newStyles = {};
		for(var i = 0; i < list.length; i++) {
			var item = list[i];
			var id = item[0];
			var css = item[1];
			var media = item[2];
			var sourceMap = item[3];
			var part = {css: css, media: media, sourceMap: sourceMap};
			if(!newStyles[id])
				styles.push(newStyles[id] = {id: id, parts: [part]});
			else
				newStyles[id].parts.push(part);
		}
		return styles;
	}

	function insertStyleElement(options, styleElement) {
		var head = getHeadElement();
		var lastStyleElementInsertedAtTop = styleElementsInsertedAtTop[styleElementsInsertedAtTop.length - 1];
		if (options.insertAt === "top") {
			if(!lastStyleElementInsertedAtTop) {
				head.insertBefore(styleElement, head.firstChild);
			} else if(lastStyleElementInsertedAtTop.nextSibling) {
				head.insertBefore(styleElement, lastStyleElementInsertedAtTop.nextSibling);
			} else {
				head.appendChild(styleElement);
			}
			styleElementsInsertedAtTop.push(styleElement);
		} else if (options.insertAt === "bottom") {
			head.appendChild(styleElement);
		} else {
			throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");
		}
	}

	function removeStyleElement(styleElement) {
		styleElement.parentNode.removeChild(styleElement);
		var idx = styleElementsInsertedAtTop.indexOf(styleElement);
		if(idx >= 0) {
			styleElementsInsertedAtTop.splice(idx, 1);
		}
	}

	function createStyleElement(options) {
		var styleElement = document.createElement("style");
		styleElement.type = "text/css";
		insertStyleElement(options, styleElement);
		return styleElement;
	}

	function createLinkElement(options) {
		var linkElement = document.createElement("link");
		linkElement.rel = "stylesheet";
		insertStyleElement(options, linkElement);
		return linkElement;
	}

	function addStyle(obj, options) {
		var styleElement, update, remove;

		if (options.singleton) {
			var styleIndex = singletonCounter++;
			styleElement = singletonElement || (singletonElement = createStyleElement(options));
			update = applyToSingletonTag.bind(null, styleElement, styleIndex, false);
			remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true);
		} else if(obj.sourceMap &&
			typeof URL === "function" &&
			typeof URL.createObjectURL === "function" &&
			typeof URL.revokeObjectURL === "function" &&
			typeof Blob === "function" &&
			typeof btoa === "function") {
			styleElement = createLinkElement(options);
			update = updateLink.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
				if(styleElement.href)
					URL.revokeObjectURL(styleElement.href);
			};
		} else {
			styleElement = createStyleElement(options);
			update = applyToTag.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
			};
		}

		update(obj);

		return function updateStyle(newObj) {
			if(newObj) {
				if(newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap)
					return;
				update(obj = newObj);
			} else {
				remove();
			}
		};
	}

	var replaceText = (function () {
		var textStore = [];

		return function (index, replacement) {
			textStore[index] = replacement;
			return textStore.filter(Boolean).join('\n');
		};
	})();

	function applyToSingletonTag(styleElement, index, remove, obj) {
		var css = remove ? "" : obj.css;

		if (styleElement.styleSheet) {
			styleElement.styleSheet.cssText = replaceText(index, css);
		} else {
			var cssNode = document.createTextNode(css);
			var childNodes = styleElement.childNodes;
			if (childNodes[index]) styleElement.removeChild(childNodes[index]);
			if (childNodes.length) {
				styleElement.insertBefore(cssNode, childNodes[index]);
			} else {
				styleElement.appendChild(cssNode);
			}
		}
	}

	function applyToTag(styleElement, obj) {
		var css = obj.css;
		var media = obj.media;

		if(media) {
			styleElement.setAttribute("media", media)
		}

		if(styleElement.styleSheet) {
			styleElement.styleSheet.cssText = css;
		} else {
			while(styleElement.firstChild) {
				styleElement.removeChild(styleElement.firstChild);
			}
			styleElement.appendChild(document.createTextNode(css));
		}
	}

	function updateLink(linkElement, obj) {
		var css = obj.css;
		var sourceMap = obj.sourceMap;

		if(sourceMap) {
			// http://stackoverflow.com/a/26603875
			css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
		}

		var blob = new Blob([css], { type: "text/css" });

		var oldSrc = linkElement.href;

		linkElement.href = URL.createObjectURL(blob);

		if(oldSrc)
			URL.revokeObjectURL(oldSrc);
	}


/***/ },
/* 19 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _react = __webpack_require__(4);

	var _react2 = _interopRequireDefault(_react);

	var _axios = __webpack_require__(7);

	var _axios2 = _interopRequireDefault(_axios);

	var _lodash = __webpack_require__(20);

	var _lodash2 = _interopRequireDefault(_lodash);

	var _reactModal = __webpack_require__(21);

	var _reactModal2 = _interopRequireDefault(_reactModal);

	var _config = __webpack_require__(8);

	var _find = __webpack_require__(22);

	var _find2 = _interopRequireDefault(_find);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var modalStyles = {
	  content: {
	    top: '50%',
	    left: '50%',
	    right: 'auto',
	    bottom: 'auto',
	    marginRight: '-50%',
	    transform: 'translate(-50%, -50%)',
	    background: '#2a2d28'
	  }
	};

	var Find = _react2.default.createClass({
	  displayName: 'Find',
	  getInitialState: function getInitialState() {
	    return {
	      games: {},
	      isModalOpen: false,
	      game: {},
	      searchValue: '',
	      feedbackText: '',
	      refreshInterval: setInterval(this.getGames, 3000)
	      // refreshSpinning: false
	    };
	  },
	  componentDidMount: function componentDidMount() {
	    this.getGames();
	  },
	  componentWillUnmount: function componentWillUnmount() {
	    clearInterval(this.state.refreshInterval);
	  },
	  handleJoinGame: function handleJoinGame(e) {
	    e.preventDefault();
	    var self = this;
	    _axios2.default.post(_config.server + '/game/custom/player', {
	      gameName: this.state.game.gameName,
	      player: this.props.username,
	      password: this.refs.gamePassword && this.refs.gamePassword.value
	    }).then(function (response) {
	      self.props.enterQueue(self.state.game);
	    }).catch(function (error) {
	      self.setState({
	        feedbackText: error.response.data
	      });
	    });
	  },
	  getGames: function getGames() {
	    // this.setState({
	    //   refreshSpinning: true
	    // })
	    var self = this;
	    _axios2.default.get(_config.server + '/game/custom/games').then(function (response) {
	      self.setState({
	        games: response.data
	        // refreshSpinning: false
	      });
	    }).catch(function (error) {
	      console.log("game error", error);
	      // self.setState({
	      //   refreshSpinning: false
	      // })
	    });
	  },
	  handleSearchChange: function handleSearchChange(e) {
	    this.setState({
	      searchValue: e.target.value
	    });
	  },
	  closeModal: function closeModal(e) {
	    e.preventDefault();
	    this.setState({
	      isModalOpen: false
	    });
	  },
	  openModal: function openModal(game) {
	    this.setState({
	      isModalOpen: true,
	      game: game
	    });
	  },
	  renderPasswordField: function renderPasswordField() {
	    if (this.state.game.gamePassword) {
	      return _react2.default.createElement(
	        'div',
	        null,
	        _react2.default.createElement('input', { placeholder: 'password', ref: 'gamePassword' }),
	        _react2.default.createElement(
	          'p',
	          { className: _find2.default.feedback },
	          this.state.feedbackText
	        )
	      );
	    }
	  },
	  renderModal: function renderModal() {
	    return _react2.default.createElement(
	      _reactModal2.default,
	      {
	        isOpen: this.state.isModalOpen,
	        onAfterOpen: this.afterOpenModal,
	        onRequestClose: this.closeModal,
	        style: modalStyles,
	        contentLabel: 'Join Game Confirmation' },
	      _react2.default.createElement(
	        'p',
	        { className: _find2.default.text },
	        'Join ',
	        this.state.game.gameName,
	        '?'
	      ),
	      _react2.default.createElement(
	        'form',
	        { onSubmit: this.handleJoinGame },
	        this.renderPasswordField(),
	        _react2.default.createElement(
	          'div',
	          null,
	          _react2.default.createElement(
	            'button',
	            { onClick: this.closeModal, className: _find2.default.cancelBtn },
	            'Cancel'
	          ),
	          _react2.default.createElement(
	            'button',
	            { className: _find2.default.joinBtn, type: 'submit' },
	            'Join'
	          )
	        )
	      )
	    );
	  },
	  renderGames: function renderGames() {
	    var _this = this;

	    var allGames = _lodash2.default.sortBy(_lodash2.default.values(this.state.games), function (game) {
	      return game.gameName;
	    });
	    var searchValue = this.state.searchValue.toLowerCase();
	    var searchedGames = _lodash2.default.filter(allGames, function (game) {
	      return game.gameName.toLowerCase().includes(searchValue) || game.creator.toLowerCase().includes(searchValue);
	    });
	    return searchedGames.map(function (game) {
	      if (game.gameState === 'Selection') return;
	      var lock = game.gamePassword ? _react2.default.createElement(
	        'td',
	        null,
	        _react2.default.createElement('img', { src: './resources/lock.png', className: _find2.default.lockIcon })
	      ) : _react2.default.createElement('td', null);
	      return _react2.default.createElement(
	        'tr',
	        { key: game.gameName, className: _find2.default.gameRow, onClick: function onClick() {
	            return _this.openModal(game);
	          } },
	        lock,
	        _react2.default.createElement(
	          'td',
	          null,
	          game.gameName
	        ),
	        _react2.default.createElement(
	          'td',
	          { className: _find2.default.td },
	          game.numTeams,
	          ' teams of ',
	          game.teamSize
	        ),
	        _react2.default.createElement(
	          'td',
	          { className: _find2.default.td },
	          game.creator
	        )
	      );
	    });
	  },
	  render: function render() {
	    return _react2.default.createElement(
	      'div',
	      { className: _find2.default.findWrapper },
	      this.renderModal(),
	      _react2.default.createElement('input', { className: _find2.default.search, placeholder: 'Search...', onChange: this.handleSearchChange }),
	      _react2.default.createElement(
	        'div',
	        { className: _find2.default.tableWrapper },
	        _react2.default.createElement(
	          'table',
	          { className: _find2.default.table },
	          _react2.default.createElement(
	            'tbody',
	            null,
	            _react2.default.createElement(
	              'tr',
	              { className: _find2.default.tr },
	              _react2.default.createElement('th', null),
	              _react2.default.createElement(
	                'th',
	                null,
	                'Name'
	              ),
	              _react2.default.createElement(
	                'th',
	                { className: _find2.default.th },
	                'Format'
	              ),
	              _react2.default.createElement(
	                'th',
	                { className: _find2.default.th },
	                'Creator'
	              )
	            ),
	            this.renderGames()
	          )
	        )
	      )
	    );
	  }
	});

	module.exports = Find;

/***/ },
/* 20 */
/***/ function(module, exports) {

	module.exports = require("lodash");

/***/ },
/* 21 */
/***/ function(module, exports) {

	module.exports = require("react-modal");

/***/ },
/* 22 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(23);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(18)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(true) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept(23, function() {
				var newContent = __webpack_require__(23);
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 23 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(16)();
	// imports
	exports.i(__webpack_require__(17), undefined);

	// module
	exports.push([module.id, ".find__tableWrapper___1rVUx {\r\n  overflow: auto;\r\n  max-height: 50vh;\r\n}\r\n\r\n.find__findWrapper___2kVpB {\r\n  width: 80%;\r\n  margin: auto;\r\n}\r\n\r\n.find__table___2yPSq{\r\n  border: 1px solid white;\r\n  border-collapse: collapse;\r\n  color: white;\r\n  width: 100%;\r\n  margin: 0 auto;\r\n  margin-bottom: 30px;\r\n}\r\n\r\n.find__td___1NeJA {\r\n  border: 1px solid white;\r\n}\r\n\r\n.find__th___3vCj2 {\r\n  padding: 10px;\r\n  border: 1px solid white;\r\n}\r\n\r\n.find__gameRow___3Zkbv {\r\n  border: 1px solid white;\r\n}\r\n\r\n.find__gameRow___3Zkbv:hover {\r\n  cursor: pointer;\r\n  background-color: " + __webpack_require__(17).locals["light-gray"] + ";\r\n}\r\n\r\n.find__joinBtn___kdNfz {\r\n  background-color: " + __webpack_require__(17).locals["green"] + ";\r\n  padding: 10px;\r\n  padding-left: 20px;\r\n  padding-right: 20px;\r\n}\r\n\r\n.find__cancelBtn___xPjz4 {\r\n  background-color: " + __webpack_require__(17).locals["red"] + ";\r\n  padding: 10px;\r\n}\r\n\r\n.find__feedback___3StKW {\r\n  font-size: 18px;\r\n  color: " + __webpack_require__(17).locals["red"] + ";\r\n  margin: 5px;\r\n}\r\n\r\n.find__text___2hnLc {\r\n  color: white;\r\n  font-size: 18px;\r\n}\r\n\r\n.find__search___XcjLS {\r\n  margin-bottom: 15px;\r\n}\r\n\r\n.find__lockIcon___QKo5P {\r\n  width: 15px;\r\n  height: 15px;\r\n  filter: invert(100%);\r\n}\r\n\r\n.find__refreshStatic___138wG, .find__refreshStatic___138wG:hover {\r\n  width: 30px;\r\n  height: 30px;\r\n  float: left;\r\n  cursor: pointer;\r\n}\r\n\r\n.find__refreshSpin___iN82f, .find__refreshSpin___iN82f:hover {\r\n  width: 30px;\r\n  height: 30px;\r\n  float: left;\r\n  cursor: pointer;\r\n  animation:find__spin___39hg6 1s linear infinite;\r\n}\r\n\r\n@keyframes find__spin___39hg6 {\r\n  100% {\r\n    transform:rotate(360deg);\r\n  }\r\n}\r\n", ""]);

	// exports
	exports.locals = {
		"colors": "\"../../../../../js/colors.css\"",
		"green": "" + __webpack_require__(17).locals["green"] + "",
		"red": "" + __webpack_require__(17).locals["red"] + "",
		"dark-blue": "" + __webpack_require__(17).locals["dark-blue"] + "",
		"blue": "" + __webpack_require__(17).locals["blue"] + "",
		"light-gray": "" + __webpack_require__(17).locals["light-gray"] + "",
		"tableWrapper": "find__tableWrapper___1rVUx",
		"findWrapper": "find__findWrapper___2kVpB",
		"table": "find__table___2yPSq",
		"td": "find__td___1NeJA",
		"th": "find__th___3vCj2",
		"gameRow": "find__gameRow___3Zkbv",
		"joinBtn": "find__joinBtn___kdNfz",
		"cancelBtn": "find__cancelBtn___xPjz4",
		"feedback": "find__feedback___3StKW",
		"text": "find__text___2hnLc",
		"search": "find__search___XcjLS",
		"lockIcon": "find__lockIcon___QKo5P",
		"refreshStatic": "find__refreshStatic___138wG",
		"refreshSpin": "find__refreshSpin___iN82f",
		"spin": "find__spin___39hg6"
	};

/***/ },
/* 24 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(25);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(18)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(true) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept(25, function() {
				var newContent = __webpack_require__(25);
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 25 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(16)();
	// imports
	exports.i(__webpack_require__(17), undefined);

	// module
	exports.push([module.id, ".custom__notSelectedBtn___3HY2X {\r\n  padding: 10px;\r\n  background-color: " + __webpack_require__(17).locals["dark-blue"] + ";\r\n  color: white;\r\n  font-size: 18px;\r\n}\r\n\r\n.custom__selectedBtn___3IyRT {\r\n  padding: 10px;\r\n  background-color: " + __webpack_require__(17).locals["blue"] + ";\r\n  color: white;\r\n  font-size: 18px;\r\n}\r\n\r\n.custom__wrapper___201iz {\r\n  margin-bottom: 20px;\r\n  margin-top: 20px;\r\n}\r\n", ""]);

	// exports
	exports.locals = {
		"colors": "\"../../../colors.css\"",
		"green": "" + __webpack_require__(17).locals["green"] + "",
		"red": "" + __webpack_require__(17).locals["red"] + "",
		"dark-blue": "" + __webpack_require__(17).locals["dark-blue"] + "",
		"blue": "" + __webpack_require__(17).locals["blue"] + "",
		"notSelectedBtn": "custom__notSelectedBtn___3HY2X",
		"selectedBtn": "custom__selectedBtn___3IyRT",
		"wrapper": "custom__wrapper___201iz"
	};

/***/ },
/* 26 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _react = __webpack_require__(4);

	var _react2 = _interopRequireDefault(_react);

	var _normal = __webpack_require__(27);

	var _normal2 = _interopRequireDefault(_normal);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var Normal = _react2.default.createClass({
	  displayName: 'Normal',
	  getInitialState: function getInitialState() {
	    return {
	      option: 'Classic'
	    };
	  },
	  optionHandler: function optionHandler(option) {
	    this.setState({
	      option: option
	    });
	  },
	  enterSoloQueue: function enterSoloQueue() {},
	  playWithFriends: function playWithFriends() {},
	  renderOptions: function renderOptions() {
	    var _this = this;

	    var options = ['Classic', 'Free-For-All'];
	    return options.map(function (option) {
	      var style = _this.state.option === option ? _normal2.default.selectedBtn : _normal2.default.notSelectedBtn;
	      return _react2.default.createElement(
	        'button',
	        { key: option, className: style, onClick: function onClick() {
	            return _this.optionHandler(option);
	          } },
	        option
	      );
	    });
	  },
	  renderText: function renderText() {
	    return {
	      'Classic': 'In Classic mode, 4 teams of 3 players each fight to be the last one standing.',
	      'Free-For-All': "In Free-For-All, 12 players duke it out until only one victor stands."
	    }[this.state.option];
	  },
	  render: function render() {
	    return _react2.default.createElement(
	      'div',
	      { className: _normal2.default.wrapper },
	      _react2.default.createElement(
	        'div',
	        { className: _normal2.default.text },
	        this.renderText()
	      ),
	      this.renderOptions(),
	      _react2.default.createElement(
	        'div',
	        null,
	        _react2.default.createElement(
	          'button',
	          { className: _normal2.default.submitBtn, onClick: this.enterSoloQueue },
	          'Enter Solo Queue'
	        ),
	        _react2.default.createElement(
	          'button',
	          { className: _normal2.default.submitBtn, onClick: this.playWithFriends },
	          'Play With Friends'
	        )
	      )
	    );
	  }
	});

	module.exports = Normal;

/***/ },
/* 27 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(28);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(18)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(true) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept(28, function() {
				var newContent = __webpack_require__(28);
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 28 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(16)();
	// imports
	exports.i(__webpack_require__(17), undefined);

	// module
	exports.push([module.id, ".normal__notSelectedBtn___28TD9 {\r\n  padding: 10px;\r\n  background-color: " + __webpack_require__(17).locals["dark-blue"] + ";\r\n  color: white;\r\n  font-size: 18px;\r\n  margin: 20px;\r\n}\r\n\r\n.normal__selectedBtn___3CFuo {\r\n  padding: 10px;\r\n  background-color: " + __webpack_require__(17).locals["blue"] + ";\r\n  color: white;\r\n  font-size: 18px;\r\n  margin: 20px;\r\n}\r\n\r\n.normal__wrapper___2Nvam {\r\n  margin-bottom: 20px;\r\n  margin-top: 20px;\r\n}\r\n\r\n.normal__text___F_YSd {\r\n  color: white;\r\n}\r\n\r\n.normal__submitBtn___oB_eP {\r\n  background-color: " + __webpack_require__(17).locals["green"] + ";\r\n  padding: 5px;\r\n  margin: 5px;\r\n  color: light-gray;\r\n  font-weight: bold;\r\n  font-size: 20px;\r\n}\r\n", ""]);

	// exports
	exports.locals = {
		"colors": "\"../../../colors.css\"",
		"green": "" + __webpack_require__(17).locals["green"] + "",
		"red": "" + __webpack_require__(17).locals["red"] + "",
		"dark-blue": "" + __webpack_require__(17).locals["dark-blue"] + "",
		"blue": "" + __webpack_require__(17).locals["blue"] + "",
		"notSelectedBtn": "normal__notSelectedBtn___28TD9",
		"selectedBtn": "normal__selectedBtn___3CFuo",
		"wrapper": "normal__wrapper___2Nvam",
		"text": "normal__text___F_YSd",
		"submitBtn": "normal__submitBtn___oB_eP"
	};

/***/ },
/* 29 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(30);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(18)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(true) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept(30, function() {
				var newContent = __webpack_require__(30);
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 30 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(16)();
	// imports
	exports.i(__webpack_require__(17), undefined);

	// module
	exports.push([module.id, ".play__notSelectedBtn___1Vnk4 {\r\n  padding: 10px;\r\n  background-color: " + __webpack_require__(17).locals["dark-blue"] + ";\r\n  color: white;\r\n  font-size: 18px;\r\n}\r\n\r\n.play__selectedBtn___3R3qx {\r\n  padding: 10px;\r\n  background-color: " + __webpack_require__(17).locals["blue"] + ";\r\n  color: white;\r\n  font-size: 18px;\r\n}\r\n\r\n.play__wrapper___1Egwx {\r\n  margin-bottom: 20px;\r\n  margin-top: 20px;\r\n}\r\n\r\n.play__blackBox___YifBv {\r\n  border: 1px solid white;\r\n  border-radius: 10px;\r\n  padding: 10px;\r\n  background-color: black;\r\n  display: inline-block;\r\n  width: 60%;\r\n  margin-top: 30px;\r\n}\r\n", ""]);

	// exports
	exports.locals = {
		"colors": "\"../../colors.css\"",
		"green": "" + __webpack_require__(17).locals["green"] + "",
		"red": "" + __webpack_require__(17).locals["red"] + "",
		"dark-blue": "" + __webpack_require__(17).locals["dark-blue"] + "",
		"blue": "" + __webpack_require__(17).locals["blue"] + "",
		"notSelectedBtn": "play__notSelectedBtn___1Vnk4",
		"selectedBtn": "play__selectedBtn___3R3qx",
		"wrapper": "play__wrapper___1Egwx",
		"blackBox": "play__blackBox___YifBv"
	};

/***/ },
/* 31 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _react = __webpack_require__(4);

	var _react2 = _interopRequireDefault(_react);

	var _axios = __webpack_require__(7);

	var _axios2 = _interopRequireDefault(_axios);

	var _ws = __webpack_require__(32);

	var _ws2 = _interopRequireDefault(_ws);

	var _lodash = __webpack_require__(20);

	var _lodash2 = _interopRequireDefault(_lodash);

	var _config = __webpack_require__(8);

	var _chat = __webpack_require__(33);

	var _chat2 = _interopRequireDefault(_chat);

	var _queue = __webpack_require__(36);

	var _queue2 = _interopRequireDefault(_queue);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var Queue = _react2.default.createClass({
	  displayName: 'Queue',


	  ///////////////////////////
	  //  COMPONENT LIFECYCLE  //
	  ///////////////////////////

	  getInitialState: function getInitialState() {
	    return {
	      game: this.props.game,
	      ws: new _ws2.default(_config.WSServer)
	    };
	  },
	  sendUpdateTrigger: function sendUpdateTrigger() {
	    // ws.send to indicate that the state has changed
	    // and everyone needs the new state for this game
	    this.state.ws.send(this.props.game.gameName);
	  },
	  componentDidMount: function componentDidMount() {
	    var self = this;
	    this.state.ws.on('open', function open() {
	      self.sendUpdateTrigger();
	    });

	    // register as listener for new game state
	    this.state.ws.on('message', function (data, flags) {
	      var game = JSON.parse(data);

	      // if not my game, disregard it
	      if (game.gameName !== self.state.game.gameName) {
	        return;
	      }

	      // if the game creator left, kick everyone out
	      if (!game.creator) {
	        self.state.ws.close();
	        self.props.leaveQueue();
	        return;
	      }

	      if (game.gameState === 'Selection') {
	        self.state.ws.close();
	        self.props.goToSelection();
	        return;
	      }

	      // since it's my game, update state
	      self.setState({
	        game: JSON.parse(data)
	      });
	    });
	  },


	  /////////////////
	  //  API CALLS  //
	  /////////////////

	  leave: function leave() {
	    var self = this;
	    _axios2.default.delete(_config.server + '/game/custom/player', {
	      params: {
	        gameName: this.props.game.gameName,
	        player: this.props.username
	      }
	    }).then(function (response) {
	      // let everyone else know that I left...
	      self.sendUpdateTrigger();
	      // close the websocket on my way out...
	      self.state.ws.close();
	      // and then leave
	      self.props.leaveQueue();
	    }).catch(function (error) {
	      console.log("leave game error", error);
	    });
	  },
	  sendMessage: function sendMessage(message) {
	    var self = this;
	    _axios2.default.post(_config.server + '/game/custom/queuechat', {
	      gameName: this.props.game.gameName,
	      player: this.props.username,
	      message: message
	    }).then(function (response) {
	      // let everyone else know that I sent a message
	      self.sendUpdateTrigger();
	    }).catch(function (error) {
	      console.log("sending queue chat error", error);
	    });
	  },
	  joinTeam: function joinTeam(teamNum) {
	    var self = this;
	    _axios2.default.post(_config.server + '/game/custom/team', {
	      gameName: this.props.game.gameName,
	      player: this.props.username,
	      teamNum: teamNum
	    }).then(function (response) {
	      // let everyone else know that I moved...
	      self.sendUpdateTrigger();
	    }).catch(function (error) {
	      console.log("move team error", error);
	    });
	  },
	  startGame: function startGame() {
	    var self = this;
	    _axios2.default.post(_config.server + '/game/custom/state', {
	      gameName: this.props.game.gameName,
	      gameState: 'Selection'
	    }).then(function (response) {
	      // let everyone else know that we're in selection gameState...
	      self.sendUpdateTrigger();
	    }).catch(function (error) {
	      console.log("start game error", error);
	    });
	  },


	  ///////////////
	  //  HELPERS  //
	  ///////////////

	  isMyTeam: function isMyTeam(teamNum) {
	    return this.state.game.teams[teamNum].includes(this.props.username);
	  },
	  isCreator: function isCreator() {
	    return this.state.game.creator == this.props.username;
	  },


	  /////////////////
	  //  RENDERING  //
	  /////////////////

	  renderBlanks: function renderBlanks(num, teamNum) {
	    if (this.isMyTeam(teamNum)) {
	      num += 1;
	    }
	    var result = [];
	    for (var i = 0; i < num; i++) {
	      result.push(_react2.default.createElement(
	        'tr',
	        { key: i },
	        _react2.default.createElement('td', { className: _queue2.default.tdEmpty })
	      ));
	    }
	    return result;
	  },
	  renderUndecided: function renderUndecided() {
	    return this.state.game.teams[0].map(function (username) {
	      return _react2.default.createElement(
	        'tr',
	        { key: username },
	        _react2.default.createElement(
	          'td',
	          { className: _queue2.default.td },
	          username
	        )
	      );
	    });
	  },
	  renderJoinTeam: function renderJoinTeam(numFrees, teamNum) {
	    var _this = this;

	    // if more people can be added and I'm not already in that team
	    if (numFrees > 0 && !this.isMyTeam(teamNum)) {
	      return _react2.default.createElement(
	        'tr',
	        null,
	        _react2.default.createElement(
	          'td',
	          { className: _queue2.default.td },
	          _react2.default.createElement(
	            'button',
	            { onClick: function onClick() {
	                return _this.joinTeam(teamNum);
	              }, className: _queue2.default.joinTeamBtn },
	            '+'
	          )
	        )
	      );
	    }
	  },
	  renderTeam: function renderTeam(team) {
	    return team.map(function (member) {
	      return _react2.default.createElement(
	        'tr',
	        { key: member },
	        _react2.default.createElement(
	          'td',
	          { className: _queue2.default.td },
	          member
	        )
	      );
	    });
	  },
	  renderTeams: function renderTeams() {
	    var _this2 = this;

	    var game = this.state.game;
	    return _lodash2.default.keys(game.teams).map(function (teamNum) {
	      if (teamNum === '0') {
	        return;
	      }
	      var team = game.teams[teamNum];
	      return _react2.default.createElement(
	        'table',
	        { key: teamNum, className: _queue2.default.table },
	        _react2.default.createElement(
	          'tbody',
	          null,
	          _react2.default.createElement(
	            'tr',
	            null,
	            _react2.default.createElement(
	              'th',
	              { className: _queue2.default.th },
	              'Team #',
	              teamNum
	            )
	          ),
	          _this2.renderTeam(team),
	          _this2.renderJoinTeam(game.teamSize - team.length, teamNum),
	          _this2.renderBlanks(game.teamSize - team.length - 1, teamNum)
	        )
	      );
	    });
	  },
	  renderTeamsList: function renderTeamsList() {
	    if (this.state.game.teamSize === 1) {
	      return;
	    }
	    return _react2.default.createElement(
	      'div',
	      { className: _queue2.default.teamsWrapper },
	      this.renderTeams()
	    );
	  },
	  renderStartBtn: function renderStartBtn() {
	    var game = this.state.game;
	    var teams = game.teams;
	    var numPlayersOnTeams = 0;
	    _lodash2.default.keys(teams).forEach(function (key) {
	      if (key !== '0') {
	        numPlayersOnTeams += teams[key].length;
	      }
	    });
	    var numPlayersNeeded = game.numTeams * game.teamSize;
	    var enabled = numPlayersNeeded === numPlayersOnTeams || game.teamSize === 1 && game.teams[0].length === numPlayersNeeded;

	    if (!this.isCreator()) {
	      var _text = enabled ? 'Waiting for game creator to start' : 'Waiting for more players';
	      return _react2.default.createElement(
	        'p',
	        { className: _queue2.default.rightText },
	        _text
	      );
	    }

	    var style = enabled ? _queue2.default.startBtn : _queue2.default.disabledBtn;
	    var text = game.teamSize === 1 && !enabled ? game.teams[0].length + '/' + numPlayersNeeded : 'Start Game';
	    return _react2.default.createElement(
	      'button',
	      { disabled: !enabled, className: style, onClick: this.startGame },
	      text
	    );
	  },
	  render: function render() {
	    var game = this.state.game;
	    var undecidedStyle = game.teamSize === 1 ? _queue2.default.singleTable : _queue2.default.table;
	    return _react2.default.createElement(
	      'div',
	      { className: _queue2.default.blackBox },
	      _react2.default.createElement(
	        'p',
	        { className: _queue2.default.text },
	        'Creator: ',
	        game.creator
	      ),
	      _react2.default.createElement(
	        'p',
	        { className: _queue2.default.text },
	        'Name: ',
	        game.gameName
	      ),
	      _react2.default.createElement(
	        'p',
	        { className: _queue2.default.text },
	        'Format: ',
	        game.numTeams,
	        ' teams of ',
	        game.teamSize
	      ),
	      _react2.default.createElement(
	        'div',
	        { className: _queue2.default.bigWrapper },
	        _react2.default.createElement(
	          'div',
	          { className: _queue2.default.teamsWrapper },
	          _react2.default.createElement(
	            'table',
	            { className: undecidedStyle },
	            _react2.default.createElement(
	              'tbody',
	              null,
	              this.renderUndecided(),
	              this.renderJoinTeam(game.numTeams * game.teamSize - game.teams[0].length, 0),
	              this.renderBlanks(game.numTeams * game.teamSize - game.teams[0].length - 1, 0)
	            )
	          )
	        ),
	        this.renderTeamsList()
	      ),
	      _react2.default.createElement(_chat2.default, { sendMessageHandler: this.sendMessage, messages: this.state.game.queueChat }),
	      _react2.default.createElement(
	        'button',
	        { className: _queue2.default.leaveBtn, onClick: this.leave },
	        'Leave Queue'
	      ),
	      this.renderStartBtn()
	    );
	  }
	});

	module.exports = Queue;

/***/ },
/* 32 */
/***/ function(module, exports) {

	module.exports = require("ws");

/***/ },
/* 33 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _react = __webpack_require__(4);

	var _react2 = _interopRequireDefault(_react);

	var _chat = __webpack_require__(34);

	var _chat2 = _interopRequireDefault(_chat);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var Chat = _react2.default.createClass({
	  displayName: 'Chat',
	  scrollToBottom: function scrollToBottom() {
	    this.lastMessage.scrollIntoView({ behavior: "smooth" });
	  },
	  componentDidMount: function componentDidMount() {
	    this.scrollToBottom();
	  },
	  componentDidUpdate: function componentDidUpdate() {
	    this.scrollToBottom();
	  },
	  handleSubmit: function handleSubmit(e) {
	    e.preventDefault();
	    this.props.sendMessageHandler(this.refs.message.value);
	    this.refs.message.value = '';
	  },
	  renderMessages: function renderMessages() {
	    return this.props.messages.map(function (message, i) {
	      return _react2.default.createElement(
	        'div',
	        { key: i },
	        _react2.default.createElement(
	          'span',
	          { className: _chat2.default.playerName },
	          message.player,
	          ': '
	        ),
	        _react2.default.createElement(
	          'span',
	          { className: _chat2.default.message },
	          message.message
	        )
	      );
	    });
	  },
	  render: function render() {
	    var _this = this;

	    return _react2.default.createElement(
	      'div',
	      { className: _chat2.default.chatWrapper },
	      _react2.default.createElement(
	        'div',
	        { className: _chat2.default.scrollPane },
	        this.renderMessages(),
	        _react2.default.createElement('div', { ref: function ref(lastMessage) {
	            return _this.lastMessage = lastMessage;
	          } })
	      ),
	      _react2.default.createElement(
	        'form',
	        { onSubmit: this.handleSubmit },
	        _react2.default.createElement(
	          'div',
	          { className: _chat2.default.formContents },
	          _react2.default.createElement('input', { type: 'text', ref: 'message', className: _chat2.default.chatInput }),
	          _react2.default.createElement('input', { type: 'submit', value: 'Send', className: _chat2.default.sendButton })
	        )
	      )
	    );
	  }
	});

	module.exports = Chat;

/***/ },
/* 34 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(35);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(18)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(true) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept(35, function() {
				var newContent = __webpack_require__(35);
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 35 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(16)();
	// imports
	exports.i(__webpack_require__(17), undefined);

	// module
	exports.push([module.id, ".chat__chatWrapper___28b0p {\r\n  margin-bottom: 15px;\r\n}\r\n\r\n.chat__scrollPane___13Znm {\r\n  height: 150px;\r\n  overflow: auto;\r\n  text-align: start;\r\n  border: 2px solid " + __webpack_require__(17).locals["dark-blue"] + ";\r\n  padding: 10px;\r\n  margin-bottom: 10px;\r\n}\r\n\r\n.chat__chatInput___1oCFk {\r\n  font-size: 14px;\r\n  width: 100%\r\n}\r\n\r\n.chat__formContents___2MGGW {\r\n  display: flex;\r\n}\r\n\r\n.chat__sendButton___38gxr {\r\n  box-shadow: none;\r\n  border-radius: 3px;\r\n  border: 0;\r\n  background-color: " + __webpack_require__(17).locals["light-blue"] + ";\r\n  font-size: 14px;\r\n}\r\n\r\n.chat__playerName___2ns4Y {\r\n  color: " + __webpack_require__(17).locals["green"] + ";\r\n}\r\n\r\n.chat__message____ilcP {\r\n  color: " + __webpack_require__(17).locals["light-blue"] + ";\r\n}\r\n", ""]);

	// exports
	exports.locals = {
		"colors": "\"../colors.css\"",
		"light-blue": "" + __webpack_require__(17).locals["light-blue"] + "",
		"dark-blue": "" + __webpack_require__(17).locals["dark-blue"] + "",
		"green": "" + __webpack_require__(17).locals["green"] + "",
		"chatWrapper": "chat__chatWrapper___28b0p",
		"scrollPane": "chat__scrollPane___13Znm",
		"chatInput": "chat__chatInput___1oCFk",
		"formContents": "chat__formContents___2MGGW",
		"sendButton": "chat__sendButton___38gxr",
		"playerName": "chat__playerName___2ns4Y",
		"message": "chat__message____ilcP"
	};

/***/ },
/* 36 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(37);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(18)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(true) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept(37, function() {
				var newContent = __webpack_require__(37);
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 37 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(16)();
	// imports
	exports.i(__webpack_require__(17), undefined);

	// module
	exports.push([module.id, ".queue__blackBox___8Fvp1 {\r\n  border: 1px solid white;\r\n  border-radius: 10px;\r\n  padding: 10px;\r\n  background-color: black;\r\n  display: inline-block;\r\n  width: 60%;\r\n  margin-top: 30px;\r\n}\r\n\r\n.queue__text___2KesJ {\r\n  color: white;\r\n  margin: 2px;\r\n}\r\n\r\n.queue__rightText___3_RG7 {\r\n  color: white;\r\n  float: right;\r\n}\r\n\r\n.queue__table___3lud8 {\r\n  border: 3px solid white;\r\n  border-collapse: collapse;\r\n  color: white;\r\n  margin: 0 auto;\r\n  margin-bottom: 15px;\r\n  width: 90%;\r\n}\r\n\r\n.queue__singleTable___23Isa {\r\n  border: 3px solid white;\r\n  border-collapse: collapse;\r\n  color: white;\r\n  margin: 0 auto;\r\n  margin-bottom: 15px;\r\n  width: 60%;\r\n}\r\n\r\n.queue__tdEmpty___3ICD9 {\r\n  padding: 10px;\r\n}\r\n\r\n.queue__th___16OC7 {\r\n  border: 1px solid white;\r\n}\r\n\r\n.queue__leaveBtn___22u01 {\r\n  background-color: " + __webpack_require__(17).locals["red"] + ";\r\n  padding: 10px;\r\n  float: left;\r\n}\r\n\r\n.queue__startBtn___2exPA {\r\n  background-color: " + __webpack_require__(17).locals["green"] + ";\r\n  padding: 10px;\r\n  float: right;\r\n}\r\n\r\n.queue__disabledBtn___3Pxbp {\r\n  background-color: gray;\r\n  padding: 10px;\r\n  float: right;\r\n  color: black;\r\n}\r\n\r\n.queue__joinTeamBtn___10wzM {\r\n  background-color: " + __webpack_require__(17).locals["green"] + ";\r\n  border-radius: 10px;\r\n}\r\n\r\n.queue__bigWrapper___3QAx8 {\r\n  display: flex;\r\n}\r\n\r\n.queue__teamsWrapper___1AM0J {\r\n  display: flex;\r\n  flex-direction: column;\r\n  flex-grow: 1;\r\n}\r\n", ""]);

	// exports
	exports.locals = {
		"colors": "\"../../colors.css\"",
		"green": "" + __webpack_require__(17).locals["green"] + "",
		"red": "" + __webpack_require__(17).locals["red"] + "",
		"dark-blue": "" + __webpack_require__(17).locals["dark-blue"] + "",
		"blue": "" + __webpack_require__(17).locals["blue"] + "",
		"blackBox": "queue__blackBox___8Fvp1",
		"text": "queue__text___2KesJ",
		"rightText": "queue__rightText___3_RG7",
		"table": "queue__table___3lud8",
		"singleTable": "queue__singleTable___23Isa",
		"tdEmpty": "queue__tdEmpty___3ICD9",
		"th": "queue__th___16OC7",
		"leaveBtn": "queue__leaveBtn___22u01",
		"startBtn": "queue__startBtn___2exPA",
		"disabledBtn": "queue__disabledBtn___3Pxbp",
		"joinTeamBtn": "queue__joinTeamBtn___10wzM",
		"bigWrapper": "queue__bigWrapper___3QAx8",
		"teamsWrapper": "queue__teamsWrapper___1AM0J"
	};

/***/ },
/* 38 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

	var _react = __webpack_require__(4);

	var _react2 = _interopRequireDefault(_react);

	var _axios = __webpack_require__(7);

	var _axios2 = _interopRequireDefault(_axios);

	var _ws = __webpack_require__(32);

	var _ws2 = _interopRequireDefault(_ws);

	var _lodash = __webpack_require__(20);

	var _lodash2 = _interopRequireDefault(_lodash);

	var _child_process = __webpack_require__(39);

	var _electron = __webpack_require__(40);

	var _fs = __webpack_require__(41);

	var _fs2 = _interopRequireDefault(_fs);

	var _config = __webpack_require__(8);

	var _chat = __webpack_require__(33);

	var _chat2 = _interopRequireDefault(_chat);

	var _selection = __webpack_require__(42);

	var _selection2 = _interopRequireDefault(_selection);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var Selection = _react2.default.createClass({
	  displayName: 'Selection',


	  ///////////////////////////
	  //  COMPONENT LIFECYCLE  //
	  ///////////////////////////

	  getInitialState: function getInitialState() {
	    return {
	      game: this.props.game,
	      lockedIn: false,
	      ws: new _ws2.default(_config.WSServer),
	      secondsRemaining: 60,
	      started: false
	    };
	  },
	  sendUpdateTrigger: function sendUpdateTrigger() {
	    // ws.send to indicate that the state has changed
	    // and everyone needs the new state for this game
	    this.state.ws.send(this.props.game.gameName);
	  },
	  componentDidMount: function componentDidMount() {
	    var _this = this;

	    var ticker = setInterval(function () {
	      if (_this.state.secondsRemaining <= 0) {
	        clearInterval(ticker);
	        _this.timeUp();
	      } else if (_this.state.started === true) {
	        clearInterval(ticker);
	      } else {
	        _this.setState({
	          secondsRemaining: _this.state.secondsRemaining - 1
	        });
	      }
	    }, 1000);

	    var self = this;
	    this.state.ws.on('open', function open() {
	      self.sendUpdateTrigger();
	    });

	    // register as listener for new game state
	    this.state.ws.on('message', function (data, flags) {
	      var game = JSON.parse(data);

	      // if not my game, disregard it
	      if (game.gameName !== self.state.game.gameName) {
	        return;
	      }

	      // since it's my game, update state
	      self.setState({
	        game: game,
	        lockedIn: game.lockedIn.includes(self.props.username)
	      });
	    });
	  },
	  componentDidUpdate: function componentDidUpdate() {
	    var _this2 = this;

	    var game = this.state.game;
	    if (game.lockedIn.length === game.numTeams * game.teamSize) {
	      var _ret = function () {
	        if (!game.port || _this2.state.started) {
	          return {
	            v: void 0
	          };
	        }
	        var runCmd = _config.runGameClient + ' ' + game.port + ' ' + _this2.props.username;
	        if (!_config.server.includes('localhost')) {
	          runCmd += ' beast';
	        }
	        console.log('runCmd', runCmd);
	        _electron.ipcRenderer.send('hideLobby');
	        var self = _this2;
	        (0, _child_process.exec)(runCmd, function (error, stdout, stderr) {
	          console.log('stdout: ' + stdout);
	          console.log('stderr: ' + stderr);
	          if (error !== null) {
	            console.log('exec error: ' + error);
	          }
	          self.props.goToPlay();
	          _electron.ipcRenderer.send('showLobby');
	        });
	        _this2.setState({
	          started: true
	        });
	      }();

	      if ((typeof _ret === 'undefined' ? 'undefined' : _typeof(_ret)) === "object") return _ret.v;
	    }
	  },


	  /////////////////
	  //  API CALLS  //
	  /////////////////

	  setSelected: function setSelected(option) {
	    if (this.state.lockedIn) {
	      return;
	    }
	    var self = this;
	    _axios2.default.post(_config.server + '/game/custom/selection', {
	      gameName: this.props.game.gameName,
	      player: this.props.username,
	      selection: option
	    }).then(function (response) {
	      // let everyone else know that I selected...
	      self.sendUpdateTrigger();
	    }).catch(function (error) {
	      console.log("selection error", error);
	    });
	  },
	  lockIn: function lockIn() {
	    var self = this;
	    _axios2.default.post(_config.server + '/game/custom/lockin', {
	      gameName: this.props.game.gameName,
	      player: this.props.username
	    }).then(function (response) {
	      // let everyone else know that I locked in...
	      self.sendUpdateTrigger();
	    }).catch(function (error) {
	      console.log("selection error", error);
	    });
	  },
	  sendMessage: function sendMessage(message) {
	    var self = this;
	    _axios2.default.post(_config.server + '/game/custom/selectionchat', {
	      gameName: this.props.game.gameName,
	      player: this.props.username,
	      teamNum: this.getMyTeamNumber(),
	      message: message
	    }).then(function (response) {
	      // let everyone else know that I locked in...
	      self.sendUpdateTrigger();
	    }).catch(function (error) {
	      console.log("selection error", error);
	    });
	  },


	  ///////////////
	  //  HELPERS  //
	  ///////////////

	  tick: function tick() {
	    this.setState({
	      secondsRemaining: this.state.secondsRemaining - 1
	    });
	  },
	  timeUp: function timeUp() {
	    var game = this.state.game;
	    var options = ['wizard', 'warrior', 'archer'];
	    if (!game.selections[this.props.username]) {
	      this.setSelected(_lodash2.default.sample(options));
	    }
	    if (!this.state.lockedIn) {
	      this.lockIn();
	    }
	  },
	  getMyTeam: function getMyTeam() {
	    var _this3 = this;

	    return _lodash2.default.flatten(_lodash2.default.filter(_lodash2.default.values(this.state.game.teams), function (team) {
	      return team.includes(_this3.props.username);
	    }));
	  },
	  getMyTeamNumber: function getMyTeamNumber() {
	    var teams = this.state.game.teams;
	    for (var key in teams) {
	      if (teams.hasOwnProperty(key)) {
	        if (teams[key].includes(this.props.username)) {
	          return key;
	        }
	      }
	    }
	    return -1;
	  },
	  getImageLocation: function getImageLocation(selection) {
	    return {
	      'question-mark': './resources/question-mark.jpg',
	      'wizard': './resources/wizardScreenShot.PNG',
	      'warrior': './resources/warriorScreenShot.PNG',
	      'archer': './resources/archerScreenShot.PNG'
	    }[selection];
	  },


	  /////////////////
	  //  RENDERING  //
	  /////////////////

	  renderTeam: function renderTeam() {
	    var _this4 = this;

	    var myTeam = this.getMyTeam();
	    return myTeam.map(function (member) {
	      var selection = _this4.state.game.selections[member] || 'question-mark';
	      var lockedInStyle = _this4.state.game.lockedIn.includes(member) ? _selection2.default.lockedInSmallImage : _selection2.default.smallImage;
	      var location = _this4.getImageLocation(selection);
	      return _react2.default.createElement(
	        'div',
	        { key: member },
	        _react2.default.createElement(
	          'p',
	          { className: _selection2.default.text },
	          member
	        ),
	        _react2.default.createElement('img', { src: location, className: lockedInStyle })
	      );
	    });
	  },
	  renderOptions: function renderOptions() {
	    var _this5 = this;

	    var options = ['wizard', 'warrior', 'archer'];
	    return options.map(function (option) {
	      var location = _this5.getImageLocation(option);
	      var lockedInStyle = _this5.state.lockedIn ? _selection2.default.lockedInImage : _selection2.default.image;
	      return _react2.default.createElement('img', {
	        key: option,
	        src: location,
	        className: lockedInStyle,
	        onClick: function onClick() {
	          return _this5.setSelected(option);
	        } });
	    });
	  },
	  renderLockInBtn: function renderLockInBtn() {
	    var style = this.state.game.selections[this.props.username] && !this.state.lockedIn ? _selection2.default.lockIn : _selection2.default.disabled;
	    return _react2.default.createElement(
	      'button',
	      { className: style, onClick: this.lockIn },
	      'Lock In'
	    );
	  },
	  renderLockedInMessage: function renderLockedInMessage() {
	    if (this.state.lockedIn) {
	      return _react2.default.createElement(
	        'p',
	        { className: _selection2.default.text },
	        'Waiting for other players to lock in'
	      );
	    }
	  },
	  render: function render() {
	    var messages = this.state.game.selectionChat[this.getMyTeamNumber()] || [];
	    return _react2.default.createElement(
	      'div',
	      { className: _selection2.default.blackBox },
	      _react2.default.createElement(
	        'p',
	        { className: _selection2.default.timer },
	        this.state.secondsRemaining
	      ),
	      _react2.default.createElement(
	        'h1',
	        { className: _selection2.default.text },
	        'Select your class'
	      ),
	      _react2.default.createElement(
	        'div',
	        { className: _selection2.default.imageWrapper },
	        this.renderTeam()
	      ),
	      _react2.default.createElement(
	        'div',
	        { className: _selection2.default.imageWrapper },
	        this.renderOptions()
	      ),
	      _react2.default.createElement(_chat2.default, { sendMessageHandler: this.sendMessage, messages: messages }),
	      this.renderLockInBtn(),
	      this.renderLockedInMessage()
	    );
	  }
	});

	module.exports = Selection;

/***/ },
/* 39 */
/***/ function(module, exports) {

	module.exports = require("child_process");

/***/ },
/* 40 */
/***/ function(module, exports) {

	module.exports = require("electron");

/***/ },
/* 41 */
/***/ function(module, exports) {

	module.exports = require("fs");

/***/ },
/* 42 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(43);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(18)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(true) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept(43, function() {
				var newContent = __webpack_require__(43);
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 43 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(16)();
	// imports
	exports.i(__webpack_require__(17), undefined);

	// module
	exports.push([module.id, ".selection__text___7RNyQ {\r\n  color: white;\r\n  margin: 20px;\r\n}\r\n\r\n.selection__timer___2EoAj {\r\n  color: white;\r\n  margin: 0;\r\n  float: left;\r\n  font-size: 40px;\r\n}\r\n\r\n.selection__blackBox___2Tifd {\r\n  border: 1px solid white;\r\n  border-radius: 10px;\r\n  padding: 10px;\r\n  background-color: black;\r\n  display: inline-block;\r\n  width: 80%;\r\n  margin-top: 30px;\r\n}\r\n\r\n.selection__imageWrapper___2VVM4 {\r\n  display: flex;\r\n  justify-content: center;\r\n}\r\n\r\n.selection__image___3ix5j {\r\n  max-width: 200px;\r\n  max-height: 200px;\r\n  margin: 10px;\r\n  border: 2px solid black;\r\n  border-radius: 10px;\r\n}\r\n\r\n.selection__lockedInImage___16gY1 {\r\n  max-width: 200px;\r\n  max-height: 200px;\r\n  margin: 10px;\r\n  border: 2px solid black;\r\n  border-radius: 10px;\r\n  filter: grayscale(70%);\r\n}\r\n\r\n.selection__image___3ix5j:hover {\r\n  border: 2px solid " + __webpack_require__(17).locals["blue"] + ";\r\n  cursor: pointer;\r\n}\r\n\r\n.selection__smallImage___2FCtw {\r\n  max-width: 100px;\r\n  max-height: 100px;\r\n  margin: 10px;\r\n  border-radius: 5px;\r\n}\r\n\r\n.selection__lockedInSmallImage___-b55b {\r\n  filter: grayscale(70%);\r\n  max-width: 100px;\r\n  max-height: 100px;\r\n  margin: 10px;\r\n  border-radius: 5px;\r\n}\r\n\r\n.selection__lockIn___1B3hX {\r\n  background-color: " + __webpack_require__(17).locals["green"] + ";\r\n  padding: 10px;\r\n  float: right;\r\n}\r\n\r\n.selection__disabled___2aMBv {\r\n  background-color: gray;\r\n  padding: 10px;\r\n  float: right;\r\n  color: black;\r\n}\r\n", ""]);

	// exports
	exports.locals = {
		"colors": "\"../../colors.css\"",
		"green": "" + __webpack_require__(17).locals["green"] + "",
		"red": "" + __webpack_require__(17).locals["red"] + "",
		"dark-blue": "" + __webpack_require__(17).locals["dark-blue"] + "",
		"blue": "" + __webpack_require__(17).locals["blue"] + "",
		"text": "selection__text___7RNyQ",
		"timer": "selection__timer___2EoAj",
		"blackBox": "selection__blackBox___2Tifd",
		"imageWrapper": "selection__imageWrapper___2VVM4",
		"image": "selection__image___3ix5j",
		"lockedInImage": "selection__lockedInImage___16gY1",
		"smallImage": "selection__smallImage___2FCtw",
		"lockedInSmallImage": "selection__lockedInSmallImage___-b55b",
		"lockIn": "selection__lockIn___1B3hX",
		"disabled": "selection__disabled___2aMBv"
	};

/***/ },
/* 44 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _react = __webpack_require__(4);

	var _react2 = _interopRequireDefault(_react);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var Learn = _react2.default.createClass({
	  displayName: 'Learn',
	  render: function render() {
	    return _react2.default.createElement(
	      'div',
	      { style: { color: 'white' } },
	      'This is where you learn stuff!'
	    );
	  }
	});

	module.exports = Learn;

/***/ },
/* 45 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(46);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(18)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(true) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept(46, function() {
				var newContent = __webpack_require__(46);
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 46 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(16)();
	// imports
	exports.i(__webpack_require__(17), undefined);

	// module
	exports.push([module.id, ".wrapper__notSelectedPage___32Cd4 {\r\n  background-color: " + __webpack_require__(17).locals["dark-blue"] + ";\r\n  color: white;\r\n  padding: 15px;\r\n}\r\n\r\n.wrapper__notSelectedPage___32Cd4:hover {\r\n  background: " + __webpack_require__(17).locals["blue"] + ";\r\n  cursor: pointer;\r\n}\r\n\r\n.wrapper__disabledPage___20q2j, disabledPage:hover {\r\n  background-color: gray;\r\n  color: white;\r\n  cursor: default;\r\n  padding: 15px;\r\n}\r\n\r\n.wrapper__selectedPage___2ngE8 {\r\n  background-color: " + __webpack_require__(17).locals["light-blue"] + ";\r\n  padding: 15px;\r\n}\r\n\r\n.wrapper__selectedPage___2ngE8:hoever {\r\n  background: " + __webpack_require__(17).locals["blue"] + ";\r\n  cursor: pointer;\r\n}\r\n\r\nul {\r\n    list-style-type: none;\r\n    margin: 0;\r\n    padding: 0;\r\n    background-color: " + __webpack_require__(17).locals["dark-blue"] + ";\r\n    display: flex;\r\n    justify-content: flex-end;\r\n}\r\n", ""]);

	// exports
	exports.locals = {
		"colors": "\"../colors.css\"",
		"blue": "" + __webpack_require__(17).locals["blue"] + "",
		"dark-blue": "" + __webpack_require__(17).locals["dark-blue"] + "",
		"light-blue": "" + __webpack_require__(17).locals["light-blue"] + "",
		"notSelectedPage": "wrapper__notSelectedPage___32Cd4",
		"disabledPage": "wrapper__disabledPage___20q2j",
		"selectedPage": "wrapper__selectedPage___2ngE8"
	};

/***/ },
/* 47 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _react = __webpack_require__(4);

	var _react2 = _interopRequireDefault(_react);

	var _axios = __webpack_require__(7);

	var _axios2 = _interopRequireDefault(_axios);

	var _config = __webpack_require__(8);

	var _login = __webpack_require__(48);

	var _login2 = _interopRequireDefault(_login);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var Lobby = _react2.default.createClass({
	  displayName: 'Lobby',
	  getInitialState: function getInitialState() {
	    return {
	      register: false,
	      feedbackText: ''
	    };
	  },
	  registerHandler: function registerHandler(e) {
	    e.preventDefault();

	    var username = this.refs.registerUsername.value;
	    var self = this;
	    _axios2.default.post(_config.server + '/auth/register', {
	      username: username,
	      password1: this.refs.registerPassword1.value,
	      password2: this.refs.registerPassword2.value,
	      email: this.refs.registerEmail.value
	    }).then(function (response) {
	      self.props.login(username);
	    }).catch(function (error) {
	      self.setState({
	        feedbackText: error.response.data
	      });
	    });
	  },
	  loginHandler: function loginHandler(e) {
	    e.preventDefault();
	    var self = this;
	    var username = this.refs.loginUsername.value;
	    _axios2.default.post(_config.server + '/auth/login', {
	      username: username,
	      password: this.refs.loginPassword.value
	    }).then(function (response) {
	      self.props.login(username);
	    }).catch(function (error) {
	      console.log(error);
	      if (error.response) {
	        self.setState({
	          feedbackText: 'incorrect username or password'
	        });
	      } else {
	        self.setState({
	          feedbackText: 'Server Down'
	        });
	      }
	    });
	  },
	  goRegister: function goRegister() {
	    this.setState({
	      register: true,
	      feedbackText: ''
	    });
	  },
	  goLogin: function goLogin() {
	    this.setState({
	      register: false,
	      feedbackText: ''
	    });
	  },
	  renderForms: function renderForms() {
	    if (this.state.register) {
	      return _react2.default.createElement(
	        'form',
	        { onSubmit: this.registerHandler, className: _login2.default.formWrapper },
	        _react2.default.createElement('input', { className: _login2.default.input, ref: 'registerUsername', placeholder: 'Username...' }),
	        _react2.default.createElement('input', { type: 'password', className: _login2.default.input, ref: 'registerPassword1', placeholder: 'Password...' }),
	        _react2.default.createElement('input', { type: 'password', className: _login2.default.input, ref: 'registerPassword2', placeholder: 'Password again...' }),
	        _react2.default.createElement('input', { className: _login2.default.input, ref: 'registerEmail', placeholder: 'Email...' }),
	        _react2.default.createElement(
	          'p',
	          { className: _login2.default.feedback },
	          this.state.feedbackText
	        ),
	        _react2.default.createElement(
	          'button',
	          { className: _login2.default.loginBtn, type: 'submit' },
	          'Register'
	        ),
	        _react2.default.createElement('hr', { className: _login2.default.hr }),
	        _react2.default.createElement(
	          'a',
	          { className: _login2.default.link, onClick: this.goLogin },
	          'Already have an account? Login.'
	        )
	      );
	    } else {
	      return _react2.default.createElement(
	        'form',
	        { onSubmit: this.loginHandler, className: _login2.default.formWrapper },
	        _react2.default.createElement('input', { className: _login2.default.input, ref: 'loginUsername', placeholder: 'Username...' }),
	        _react2.default.createElement('input', { type: 'password', className: _login2.default.input, ref: 'loginPassword', placeholder: 'Password...' }),
	        _react2.default.createElement(
	          'p',
	          { className: _login2.default.feedback },
	          this.state.feedbackText
	        ),
	        _react2.default.createElement(
	          'button',
	          { className: _login2.default.loginBtn, type: 'submit' },
	          'Login'
	        ),
	        _react2.default.createElement('hr', { className: _login2.default.hr }),
	        _react2.default.createElement(
	          'a',
	          { className: _login2.default.link },
	          'Forgot username or password?'
	        ),
	        _react2.default.createElement(
	          'a',
	          { className: _login2.default.link, onClick: this.goRegister },
	          'New user? Create an account.'
	        )
	      );
	    }
	  },
	  render: function render() {
	    return _react2.default.createElement(
	      'div',
	      null,
	      _react2.default.createElement(
	        'div',
	        { className: _login2.default.slideInFromTop },
	        _react2.default.createElement('img', { className: _login2.default.logoImg, src: 'resources/logo2.png' })
	      ),
	      _react2.default.createElement(
	        'div',
	        { className: _login2.default.wrapper },
	        this.renderForms()
	      )
	    );
	  }
	});

	module.exports = Lobby;

/***/ },
/* 48 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(49);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(18)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(true) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept(49, function() {
				var newContent = __webpack_require__(49);
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 49 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(16)();
	// imports
	exports.i(__webpack_require__(17), undefined);

	// module
	exports.push([module.id, ".login__header___lRq8W {\r\n  margin: 0;\r\n  padding: 30px;\r\n}\r\n\r\n.login__formWrapper___Qh_qI {\r\n  display: flex;\r\n  flex-direction: column;\r\n  border: 1px solid white;\r\n  border-radius: 10px;\r\n  margin: 20px;\r\n  padding: 10px;\r\n  background-color: black;\r\n}\r\n\r\n.login__loginBtn___3XN8r {\r\n  background-color: " + __webpack_require__(17).locals["green"] + ";\r\n  margin: 10px;\r\n  padding: 10px;\r\n  font-size: 20px;\r\n  margin-left: auto;\r\n}\r\n\r\n.login__feedback___3eBIM {\r\n  font-size: 18px;\r\n  color: red;\r\n  margin: 0;\r\n}\r\n\r\n.login__input___1Ch4B {\r\n  margin: 10px;\r\n}\r\n\r\n.login__link___3laYo {\r\n  color: " + __webpack_require__(17).locals["light-blue"] + ";\r\n  margin-right: auto;\r\n  margin-left: 10px;\r\n}\r\n\r\n.login__link___3laYo:hover {\r\n  cursor: pointer;\r\n  color: " + __webpack_require__(17).locals["lighter-blue"] + ";\r\n}\r\n\r\n.login__hr___1z88W  {\r\n  width: 100%;\r\n}\r\n\r\n.login__slideInFromTop___2m26- {\r\n  animation-duration: 3s;\r\n  animation-name: login__slideInFromTop___2m26-;\r\n  position: relative;\r\n}\r\n\r\n.login__logoImg___1iSy- {\r\n  max-width: 500px;\r\n}\r\n\r\n@keyframes login__slideInFromTop___2m26- {\r\n  from {\r\n    top: -200px;\r\n  }\r\n\r\n  to {\r\n    top: 0px;\r\n  }\r\n}\r\n\r\n\r\n.login__wrapper___3eVm7 {\r\n  padding: 20px;\r\n  display: flex;\r\n  justify-content: flex-start;\r\n  animation-duration: 3s;\r\n  animation-name: login__slideInFromBottom___1uokV;\r\n  position: relative;\r\n}\r\n\r\n@keyframes login__slideInFromBottom___1uokV {\r\n  from {\r\n    bottom: -1000px;\r\n  }\r\n\r\n  to {\r\n    bottom: 0px;\r\n  }\r\n}\r\n", ""]);

	// exports
	exports.locals = {
		"colors": "\"../colors.css\"",
		"green": "" + __webpack_require__(17).locals["green"] + "",
		"light-blue": "" + __webpack_require__(17).locals["light-blue"] + "",
		"lighter-blue": "" + __webpack_require__(17).locals["lighter-blue"] + "",
		"dark-gray": "" + __webpack_require__(17).locals["dark-gray"] + "",
		"header": "login__header___lRq8W",
		"formWrapper": "login__formWrapper___Qh_qI",
		"loginBtn": "login__loginBtn___3XN8r",
		"feedback": "login__feedback___3eBIM",
		"input": "login__input___1Ch4B",
		"link": "login__link___3laYo",
		"hr": "login__hr___1z88W",
		"slideInFromTop": "login__slideInFromTop___2m26-",
		"logoImg": "login__logoImg___1iSy-",
		"wrapper": "login__wrapper___3eVm7",
		"slideInFromBottom": "login__slideInFromBottom___1uokV"
	};

/***/ },
/* 50 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(51);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(18)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(true) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept(51, function() {
				var newContent = __webpack_require__(51);
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 51 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(16)();
	// imports
	exports.i(__webpack_require__(17), undefined);

	// module
	exports.push([module.id, "html {\r\n  background-color: " + __webpack_require__(17).locals["dark-gray"] + ";\r\n  text-align: center;\r\n  font-family: 'Roboto', sans-serif;\r\n  height: 100%;\r\n}\r\n\r\nbody {\r\n  background-size: cover;\r\n  height: 100%;\r\n  margin: 0px;\r\n  overflow: hidden;\r\n}\r\n\r\nh1 {\r\n  text-align: center;\r\n  color: " + __webpack_require__(17).locals["very-light-gray"] + ";\r\n  margin-bottom: 10px;\r\n}\r\n\r\ninput {\r\n  font-size: 24px;\r\n  margin: 1px;\r\n  border-radius: 3px;\r\n}\r\n\r\nbutton {\r\n  box-shadow: none;\r\n  border-radius: 3px;\r\n  border: 0;\r\n}\r\n\r\nbutton:hover {\r\n  cursor: pointer;\r\n}\r\n\r\n*:focus {\r\n    outline: none;\r\n}\r\n", ""]);

	// exports
	exports.locals = {
		"colors": "\"./colors.css\"",
		"green": "" + __webpack_require__(17).locals["green"] + "",
		"dark-gray": "" + __webpack_require__(17).locals["dark-gray"] + "",
		"light-gray": "" + __webpack_require__(17).locals["light-gray"] + "",
		"very-light-gray": "" + __webpack_require__(17).locals["very-light-gray"] + "",
		"blue": "" + __webpack_require__(17).locals["blue"] + "",
		"light-blue": "" + __webpack_require__(17).locals["light-blue"] + "",
		"dark-blue": "" + __webpack_require__(17).locals["dark-blue"] + ""
	};

/***/ }
/******/ ]);