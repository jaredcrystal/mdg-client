import platform from 'platform'

module.exports = {

  ///////////////////
  //  DEVELOPMENT  //
  ///////////////////

  // server: 'http://localhost:3030',
  // WSServer: 'ws://localhost:3030',
  // runGameClient: {
  //   'linux': './executables/LinuxClient.x86_64',
  //   'win32': 'executables\\client.exe',
  // }[platform.os.family.toLowerCase()],


  //////////////////
  //  PRODUCTION  //
  //////////////////

  server: 'http://138.68.53.198:3030',
  WSServer: 'ws://138.68.53.198:3030',
  // server: 'http://192.168.10.140:3030',
  // WSServer: 'ws://192.168.10.140:3030',
  runGameClient: {
    'linux': './resources/app/executables/LinuxClient.x86_64',
    'win32': 'resources\\app\\executables\\client.exe'
  }[platform.os.family.toLowerCase()],

}
