var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var combineLoaders = require('webpack-combine-loaders');
var nodeExternals = require('webpack-node-externals');
var values = require('postcss-modules-values');
const { resolve } = require('path')


module.exports = {
  resolve: {
    alias: {
      'appCommon': resolve(__dirname, 'js/common')
    }
  },
  entry: {
    app: ['webpack/hot/dev-server', './js/entry.js'],
  },
  output: {
    path: './build',
    filename: 'bundle.js',
    publicPath: 'http://localhost:8080/built/'
  },
  target: 'node',
  externals: [nodeExternals({
    whitelist: ['webpack/hot/dev-server']
  })],
  devServer: {
    contentBase: './public',
    publicPath: 'http://localhost:8080/built/'
  },
  module: {
   loaders: [
     { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/ },
     { test: /\.json$/, loader: "json-loader" },
     { test: /\.(png|jpg|svg)$/, loader: 'url?limit=25000' },
     {
        test: /\.css$/,
        loader: combineLoaders([
          {
            loader: 'style-loader'
          }, {
            loader: 'css-loader',
            query: {
              modules: true,
              localIdentName: '[name]__[local]___[hash:base64:5]'
            }
          }
        ])
     }
   ]
  },
  postcss: [values],
   plugins: [
     new webpack.HotModuleReplacementPlugin(),
     new ExtractTextPlugin('style.css', {allChunks: true})
   ]
}
