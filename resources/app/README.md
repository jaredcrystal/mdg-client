### To run the lobby client for development

1. set `config.js` to development
1. toggle comments in `index.html` to use the localhost tags instead of the bundle.js tag (bundle is for production)
1. run `npm install`
1. in one terminal: `npm run watch`
1. in another terminal: `npm start`

### Configurations

(Only if you want to run the game from the lobby): You'll need to put the client executable in the `executables` directory. You will need to update the `config.js` file with the correct name of the executable.

config.js contains the location of the lobby server. You can choose to use a local lobby-server or the production server when developing.

## Production Package Distribution

1. make sure config.js is set to production
1. run `webpack` to generate `build/bundle.js` (webpack needs to be installed globally for this command)
1. toggle script comments in index.html to use `bundle.js` instead of localhost scripts
1. run `npm install` if not already run
1. run `node packager.js`
