'use strict';
var packager = require('electron-packager');
var options = {
    'arch': 'x64',
    'platform': 'darwin',
    'dir': '.',
    // 'app-copyright': 'Jared Crystal',
    'app-version': '0.0.1',
    // 'asar': true,
    // 'icon': './app.ico',
    'name': 'MDG',
    'out': '.',
    'overwrite': true,
    'prune': true,
    'version': '1.4.6'
};
packager(options, function done_callback(err, appPaths) {
    console.log("Error: ", err);
    console.log("appPaths: ", appPaths);
});