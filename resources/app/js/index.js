import React from 'react'
import axios from 'axios'
import {server} from '../config.js'
import Wrapper from './index/wrapper.js'
import Login from './index/login.js'
import styles from './index.css'

// This file to handle routing via state
const Index = React.createClass({

  getInitialState() {
    return {
      loggedIn: false,
      username: 'admin'
    }
  },

  login(username) {
    this.setState({
      loggedIn: true,
      username: username
    })
  },

  logout() {
    this.setState({
      loggedIn: false,
      username: ''
    })
  },

  render() {
    if (this.state.loggedIn) {
      return ( <Wrapper logout={this.logout} username={this.state.username} />)
    }
    return ( <Login login={this.login}/>)
  }
})

module.exports = Index
