import React from 'react'

import styles from './chat.css'

const Chat = React.createClass({

  scrollToBottom() {
    this.lastMessage.scrollIntoView({behavior: "smooth"})
  },

  componentDidMount() {
      this.scrollToBottom();
  },

  componentDidUpdate() {
      this.scrollToBottom();
  },

  handleSubmit(e) {
    e.preventDefault()
    this.props.sendMessageHandler(this.refs.message.value)
    this.refs.message.value = ''
  },

  renderMessages() {
    return this.props.messages.map((message, i) => {
      return (
        <div key={i}>
          <span className={styles.playerName}>{message.player}: </span>
          <span className={styles.message}>{message.message}</span>
        </div>
      )
    })
  },

  render() {
    return (
      <div className={styles.chatWrapper}>
        <div className={styles.scrollPane}>
          {this.renderMessages()}
          <div ref={lastMessage => this.lastMessage = lastMessage} />
        </div>
        <form onSubmit={this.handleSubmit}>
          <div className={styles.formContents}>
            <input type="text" ref="message" className={styles.chatInput}/>
            <input type="submit" value="Send" className={styles.sendButton}/>
          </div>
        </form>
      </div>
    )
  }
})

module.exports = Chat
