import React from 'react'
import axios from 'axios'
import {server} from '../../config.js'

import styles from './login.css'

const Lobby = React.createClass({

  getInitialState() {
    return {
      register: false,
      feedbackText: ''
    }
  },

  registerHandler(e) {
    e.preventDefault()

    const username = this.refs.registerUsername.value
    const self = this
    axios.post(server + '/auth/register', {
      username: username,
      password1: this.refs.registerPassword1.value,
      password2: this.refs.registerPassword2.value,
      email: this.refs.registerEmail.value
    })
    .then(function (response) {
      self.props.login(username)
    })
    .catch(function (error) {
      self.setState({
        feedbackText: error.response.data
      })
    })
  },

  loginHandler(e) {
    e.preventDefault()
    const self = this
    const username = this.refs.loginUsername.value
    axios.post(server + '/auth/login', {
      username: username,
      password: this.refs.loginPassword.value
    })
    .then(function (response) {
      self.props.login(username)
    })
    .catch(function (error) {
      console.log(error)
      if(error.response) {
        self.setState({
          feedbackText: 'incorrect username or password'
        })
      }else {
        self.setState({
          feedbackText: 'Server Down'
        })
      }
    })
  },

  goRegister() {
    this.setState({
      register: true,
      feedbackText: ''
    })
  },

  goLogin() {
    this.setState({
      register: false,
      feedbackText: ''
    })
  },

  renderForms() {
    if (this.state.register){
      return (
        <form onSubmit={this.registerHandler} className={styles.formWrapper}>
          <input className={styles.input} ref="registerUsername" placeholder="Username..." />
          <input type="password" className={styles.input} ref="registerPassword1" placeholder="Password..." />
          <input type="password" className={styles.input} ref="registerPassword2" placeholder="Password again..." />
          <input className={styles.input} ref="registerEmail" placeholder="Email..." />
          <p className={styles.feedback}>{this.state.feedbackText}</p>
          <button className={styles.loginBtn} type="submit">Register</button>
          <hr className={styles.hr} />
          <a className={styles.link} onClick={this.goLogin}>Already have an account? Login.</a>
        </form>
      )
    } else {
      return (
        <form onSubmit={this.loginHandler} className={styles.formWrapper}>
          <input className={styles.input} ref="loginUsername" placeholder="Username..." />
          <input type="password" className={styles.input} ref="loginPassword" placeholder="Password..." />
          <p className={styles.feedback}>{this.state.feedbackText}</p>
          <button className={styles.loginBtn} type="submit">Login</button>
          <hr className={styles.hr} />
          <a className={styles.link}>Forgot username or password?</a>
          <a className={styles.link} onClick={this.goRegister}>New user? Create an account.</a>
        </form>
      )
    }
  },

  render() {
    return (
      <div>
        <div className={styles.slideInFromTop}><img className={styles.logoImg} src="resources/logo2.png"/></div>
        <div className={styles.wrapper}>
          {this.renderForms()}
        </div>
      </div>
    )
  }
})

module.exports = Lobby
