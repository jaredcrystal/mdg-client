import React from 'react'
import axios from 'axios'
import {server, runGameClient} from '../../../config.js'
import Custom from './play/custom.js'
import Normal from './play/normal.js'

import styles from './play.css'

const Lobby = React.createClass({

  getInitialState() {
    return {
      gameType: 'Custom'
    }
  },

  gameTypeHandler(type) {
    this.setState({
      gameType: type
    })
  },

  renderGameType() {
    return {
      'Normal': <Normal username={this.props.username} />,
      'Custom': <Custom username={this.props.username} enterQueue={this.props.enterQueue} />
    }[this.state.gameType]
  },

  renderGameTypeButtons() {
    const gameTypes = ['Normal', 'Custom']
    return gameTypes.map((type) => {
      const style = this.state.gameType === type ? styles.selectedBtn : styles.notSelectedBtn
      return (
        <button key={type} className={style} onClick={() => this.gameTypeHandler(type)}>
          {type}
        </button>
      )
    })
  },

  render() {
    return (
      <div className={styles.blackBox}>
        <div className={styles.wrapper}>
          {this.renderGameTypeButtons()}
        </div>
          {this.renderGameType()}
      </div>
    )
  }
})

module.exports = Lobby
