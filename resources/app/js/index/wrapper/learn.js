import React from 'react'

const Learn = React.createClass({

  render() {
    return (
      <div style={{color: 'white'}}>
        This is where you learn stuff!
      </div>
    )
  }
})

module.exports = Learn
