import React from 'react'
import axios from 'axios'
import WebSocket from 'ws'
import _ from 'lodash'
import {exec} from 'child_process'
import {ipcRenderer} from 'electron'
import fs from 'fs'
import {server, WSServer, runGameClient} from '../../../config.js'

import Chat from 'appCommon/chat'

import styles from './selection.css'

const Selection = React.createClass({

  ///////////////////////////
  //  COMPONENT LIFECYCLE  //
  ///////////////////////////

  getInitialState() {
    return {
      game: this.props.game,
      lockedIn: false,
      ws: new WebSocket(WSServer),
      secondsRemaining: 60,
      started: false
    }
  },

  sendUpdateTrigger() {
    // ws.send to indicate that the state has changed
    // and everyone needs the new state for this game
    this.state.ws.send(this.props.game.gameName)
  },

  componentDidMount() {
    const ticker = setInterval(() => {
      if (this.state.secondsRemaining <= 0) {
        clearInterval(ticker)
        this.timeUp()
      } else if (this.state.started === true) {
        clearInterval(ticker)
      }else {
        this.setState({
          secondsRemaining: this.state.secondsRemaining - 1
        })
      }
    }, 1000)

    const self = this
    this.state.ws.on('open', function open() {
      self.sendUpdateTrigger()
    });

    // register as listener for new game state
    this.state.ws.on('message', function(data, flags) {
      const game = JSON.parse(data)

      // if not my game, disregard it
      if (game.gameName !== self.state.game.gameName) {
        return
      }

      // since it's my game, update state
      self.setState({
        game: game,
        lockedIn: game.lockedIn.includes(self.props.username)
      })
    });
  },

  componentDidUpdate() {
    const game = this.state.game
    if (game.lockedIn.length === (game.numTeams * game.teamSize)) {
      if (!game.port || this.state.started) {
        return
      }
      let runCmd = runGameClient + ' ' + game.port + ' ' + this.props.username
      if (!server.includes('localhost')) {
        runCmd += ' beast'
      }
      console.log('runCmd', runCmd)
      ipcRenderer.send('hideLobby')
      const self = this
      exec(runCmd, function (error, stdout, stderr) {
          console.log('stdout: ' + stdout);
          console.log('stderr: ' + stderr);
          if (error !== null) {
               console.log('exec error: ' + error);
          }
          self.props.goToPlay()
          ipcRenderer.send('showLobby')
      });
      this.setState({
        started: true
      })
    }
  },

  /////////////////
  //  API CALLS  //
  /////////////////

  setSelected(option) {
    if (this.state.lockedIn) {
      return
    }
    const self = this
    axios.post(server + '/game/custom/selection', {
      gameName: this.props.game.gameName,
      player: this.props.username,
      selection: option
    })
    .then(function (response) {
      // let everyone else know that I selected...
      self.sendUpdateTrigger()
    })
    .catch(function (error) {
      console.log("selection error", error)
    })
  },

  lockIn() {
    const self = this
    axios.post(server + '/game/custom/lockin', {
      gameName: this.props.game.gameName,
      player: this.props.username
    })
    .then(function (response) {
      // let everyone else know that I locked in...
      self.sendUpdateTrigger()
    })
    .catch(function (error) {
      console.log("selection error", error)
    })
  },

  sendMessage(message) {
    const self = this
    axios.post(server + '/game/custom/selectionchat', {
      gameName: this.props.game.gameName,
      player: this.props.username,
      teamNum: this.getMyTeamNumber(),
      message: message
    })
    .then(function (response) {
      // let everyone else know that I locked in...
      self.sendUpdateTrigger()
    })
    .catch(function (error) {
      console.log("selection error", error)
    })
  },

  ///////////////
  //  HELPERS  //
  ///////////////

  tick() {
    this.setState({
      secondsRemaining: this.state.secondsRemaining - 1
    })
  },

  timeUp() {
    const game = this.state.game
    const options = ['wizard', 'warrior', 'archer']
    if (!game.selections[this.props.username]) {
      this.setSelected(_.sample(options))
    }
    if (!this.state.lockedIn) {
      this.lockIn()
    }
  },

  getMyTeam() {
    return _.flatten(_.filter(_.values(this.state.game.teams), (team) => {
      return team.includes(this.props.username)
    }))
  },

  getMyTeamNumber() {
    const teams = this.state.game.teams
    for (var key in teams) {
      if (teams.hasOwnProperty(key)) {
        if (teams[key].includes(this.props.username)) {
          return key
        }
      }
    }
    return -1
  },

  getImageLocation(selection) {
    return {
      'question-mark': './resources/question-mark.jpg',
      'wizard': './resources/wizardScreenShot.PNG',
      'warrior': './resources/warriorScreenShot.PNG',
      'archer': './resources/archerScreenShot.PNG'
    }[selection]
  },

  /////////////////
  //  RENDERING  //
  /////////////////

  renderTeam() {
    const myTeam = this.getMyTeam()
    return myTeam.map((member) => {
      const selection = this.state.game.selections[member] || 'question-mark'
      const lockedInStyle = this.state.game.lockedIn.includes(member) ? styles.lockedInSmallImage : styles.smallImage
      const location = this.getImageLocation(selection)
      return (
        <div key={member}>
          <p className={styles.text}>{member}</p>
          <img src={location} className={lockedInStyle} />
        </div>
      )
    })
  },

  renderOptions() {
    const options = ['wizard', 'warrior', 'archer']
    return options.map((option) => {
      const location = this.getImageLocation(option)
      const lockedInStyle = this.state.lockedIn ? styles.lockedInImage : styles.image
      return (
        <img
          key={option}
          src={location}
          className={lockedInStyle}
          onClick={() => this.setSelected(option)} />
      )
    })

  },

  renderLockInBtn() {
    const style = this.state.game.selections[this.props.username]  && !this.state.lockedIn ? styles.lockIn : styles.disabled
    return (
      <button className={style} onClick={this.lockIn}>Lock In</button>
    )
  },

  renderLockedInMessage() {
    if (this.state.lockedIn) {
      return (
        <p className={styles.text}>Waiting for other players to lock in</p>
      )
    }
  },

  render() {
    const messages = this.state.game.selectionChat[this.getMyTeamNumber()] || []
    return (
      <div className={styles.blackBox}>
        <p className={styles.timer}>{this.state.secondsRemaining}</p>
        <h1 className={styles.text}>Select your class</h1>
        <div className={styles.imageWrapper}>
          {this.renderTeam()}
        </div>
        <div className={styles.imageWrapper}>
          {this.renderOptions()}
        </div>
        <Chat sendMessageHandler={this.sendMessage} messages={messages} />
        {this.renderLockInBtn()}
        {this.renderLockedInMessage()}
      </div>
    )
  }
})

module.exports = Selection
