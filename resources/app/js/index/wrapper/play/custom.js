import React from 'react'
import Create from './custom/create.js'
import Find from './custom/find.js'

import styles from './custom.css'

const Lobby = React.createClass({

  getInitialState() {
    return {
      customType: 'Find'
    }
  },

  customTypeHandler(type) {
    this.setState({
      customType: type
    })
  },

  renderCustomType() {
    return {
      'Create': <Create username={this.props.username} enterQueue={this.props.enterQueue} />,
      'Find': <Find username={this.props.username} enterQueue={this.props.enterQueue} />
    }[this.state.customType]
  },

  renderCustomTypeButtons() {
    const gameTypes = ['Create', 'Find']
    return gameTypes.map((type) => {
      const style = this.state.customType === type ? styles.selectedBtn : styles.notSelectedBtn
      return (
        <button key={type} className={style} onClick={() => this.customTypeHandler(type)}>
          {type}
        </button>
      )
    })
  },

  render() {
    return (
      <div>
        <div className={styles.wrapper}>
          {this.renderCustomTypeButtons()}
        </div>
          {this.renderCustomType()}
      </div>
    )
  }
})

module.exports = Lobby
