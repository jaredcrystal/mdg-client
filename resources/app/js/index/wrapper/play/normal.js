import React from 'react'

import styles from './normal.css'

const Normal = React.createClass({

  getInitialState() {
    return {
      option: 'Classic'
    }
  },

  optionHandler(option) {
    this.setState({
      option: option
    })
  },

  enterSoloQueue() {

  },

  playWithFriends() {

  },

  renderOptions() {
    const options = ['Classic', 'Free-For-All']
    return options.map((option) => {
      const style = this.state.option === option ? styles.selectedBtn : styles.notSelectedBtn
      return (
        <button key={option} className={style} onClick={() => this.optionHandler(option)}>
          {option}
        </button>
      )
    })
  },

  renderText() {
    return {
      'Classic': 'In Classic mode, 4 teams of 3 players each fight to be the last one standing.',
      'Free-For-All': "In Free-For-All, 12 players duke it out until only one victor stands."
    }[this.state.option]
  },

  render() {
    return (
      <div className={styles.wrapper}>
        <div className={styles.text}>
          {this.renderText()}
        </div>
        {this.renderOptions()}
        <div>
          <button className={styles.submitBtn} onClick={this.enterSoloQueue}>
            Enter Solo Queue
          </button>
          <button className={styles.submitBtn} onClick={this.playWithFriends}>
            Play With Friends
          </button>
        </div>
      </div>
    )
  }
})

module.exports = Normal
