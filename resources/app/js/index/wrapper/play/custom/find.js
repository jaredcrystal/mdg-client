import React from 'react'
import axios from 'axios'
import _ from 'lodash'
import Modal from 'react-modal'

import {server} from '../../../../../config.js'

import styles from './find.css'

const modalStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    background: '#2a2d28'
  }
};

const Find = React.createClass({

  getInitialState() {
    return {
      games: {},
      isModalOpen: false,
      game: {},
      searchValue: '',
      feedbackText: '',
      refreshInterval: setInterval(this.getGames, 3000)
      // refreshSpinning: false
    }
  },

  componentDidMount() {
    this.getGames()
  },

  componentWillUnmount() {
    clearInterval(this.state.refreshInterval)
  },

  handleJoinGame(e) {
    e.preventDefault()
    const self = this
    axios.post(server + '/game/custom/player', {
      gameName: this.state.game.gameName,
      player: this.props.username,
      password: this.refs.gamePassword && this.refs.gamePassword.value
    })
    .then(function (response) {
      self.props.enterQueue(self.state.game)
    })
    .catch(function (error) {
      self.setState({
        feedbackText: error.response.data
      })
    })
  },

  getGames() {
    // this.setState({
    //   refreshSpinning: true
    // })
    const self = this
    axios.get(server + '/game/custom/games')
    .then(function (response) {
      self.setState({
        games: response.data
        // refreshSpinning: false
      })
    })
    .catch(function (error) {
      console.log("game error", error)
      // self.setState({
      //   refreshSpinning: false
      // })
    })
  },

  handleSearchChange(e) {
    this.setState({
      searchValue: e.target.value
    })
  },

  closeModal(e) {
    e.preventDefault()
    this.setState({
      isModalOpen: false
    })
  },

  openModal(game) {
    this.setState({
      isModalOpen: true,
      game: game
    })
  },

  renderPasswordField() {
    if (this.state.game.gamePassword) {
      return (
        <div>
          <input placeholder="password" ref="gamePassword" />
          <p className={styles.feedback}>{this.state.feedbackText}</p>
        </div>

      )
    }
  },

  renderModal() {
    return (
      <Modal
        isOpen={this.state.isModalOpen}
        onAfterOpen={this.afterOpenModal}
        onRequestClose={this.closeModal}
        style={modalStyles}
        contentLabel="Join Game Confirmation" >

        <p className={styles.text}>Join {this.state.game.gameName}?</p>
        <form onSubmit={this.handleJoinGame}>
          {this.renderPasswordField()}
          <div>
            <button onClick={this.closeModal} className={styles.cancelBtn}>Cancel</button>
            <button className={styles.joinBtn} type="submit">Join</button>
          </div>
        </form>
      </Modal>
    )
  },

  renderGames() {
    const allGames = _.sortBy(_.values(this.state.games), (game) => game.gameName)
    const searchValue = this.state.searchValue.toLowerCase()
    const searchedGames = _.filter(allGames, (game) => {
      return game.gameName.toLowerCase().includes(searchValue) || game.creator.toLowerCase().includes(searchValue)
    })
    return searchedGames.map((game) => {
      if (game.gameState === 'Selection') return
      const lock = game.gamePassword ? <td><img src="./resources/lock.png" className={styles.lockIcon} /></td> : <td></td>
      return (
        <tr key={game.gameName} className={styles.gameRow} onClick={() => this.openModal(game)}>
          {lock}
          <td>{game.gameName}</td>
          <td className={styles.td}>{game.numTeams} teams of {game.teamSize}</td>
          <td className={styles.td}>{game.creator}</td>
        </tr>
      )
    })
  },

  render() {
    // const refreshStyle = this.state.refreshSpinning ? styles.refreshSpin : styles.refreshStatic
    return (
      <div className={styles.findWrapper}>
        {this.renderModal()}
        {/*
        <a onClick={this.getGames}>
          <img src="./resources/refresh_icon.png" className={refreshStyle} />
        </a>
        */}
        <input className={styles.search} placeholder="Search..." onChange={this.handleSearchChange} />
        <div className={styles.tableWrapper}>
          <table className={styles.table}>
            <tbody>
              <tr className={styles.tr}>
                <th></th>
                <th>Name</th>
                <th className={styles.th}>Format</th>
                <th className={styles.th}>Creator</th>
              </tr>
              {this.renderGames()}
            </tbody>
          </table>
        </div>
      </div>
    )
  }
})

module.exports = Find
