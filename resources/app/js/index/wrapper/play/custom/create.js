import React from 'react'
import axios from 'axios'
import {server} from '../../../../../config.js'

import styles from './create.css'

const Create = React.createClass({

  getInitialState() {
    return {
      numTeams: 4,
      teamSize: 3,
      feedbackText: ''
    }
  },

  handleTeamSize(size) {
    this.setState({
      teamSize: size,
      numTeams: 2
    })
  },

  handleNumTeams(num) {
    this.setState({
      numTeams: num
    })
  },

  createGameHandler() {
    const self = this
    axios.post(server + '/game/custom/create', {
      creator: this.props.username,
      gameName: this.refs.gameName.value.trim(),
      gamePassword: this.refs.gamePassword.value,
      numTeams: this.state.numTeams,
      teamSize: this.state.teamSize
    })
    .then(function (response) {
      self.props.enterQueue(response.data)
    })
    .catch(function (error) {
      console.log(error)
      self.setState({
        feedbackText: error.response.data
      })
    })
  },

  renderTeamSizes() {
    const teamSizes = [1,2,3,4,5,6]
    return teamSizes.map((size) => {
      const btnStyle = this.state.teamSize === size ? styles.selectedCircle : styles.notSelectedCircle
      return (
        <button key={size} className={btnStyle} onClick={() => this.handleTeamSize(size)}>
          {size}
        </button>
      )
    })
  },

  renderNumTeams() {
    const numTeams = {
      1: [1,2,3,4,5,6,7,8,9,10,11,12],
      2: [1,2,3,4,5,6],
      3: [1,2,3,4],
      4: [1,2,3],
      5: [1,2],
      6: [1,2]
    }[this.state.teamSize]

    return numTeams.map((num) => {
      const btnStyle = this.state.numTeams === num ? styles.selectedCircle : styles.notSelectedCircle
      return (
        <button key={num} className={btnStyle} onClick={() => this.handleNumTeams(num)}>
          {num}
        </button>
      )
    })
  },

  render() {
    return (
      <div>
        <div><input ref="gameName" placeholder="Game Name" /></div>
        <div><input ref="gamePassword" placeholder="Password (optional)" /></div>
        <div>
          <p className={styles.textLabel}>Team Size:</p>
          {this.renderTeamSizes()}
        </div>
        <div>
          <p className={styles.textLabel}>Number of Teams:</p>
          {this.renderNumTeams()}
        </div>
        <p className={styles.feedback}>{this.state.feedbackText}</p>
        <button className={styles.submitBtn} onClick={this.createGameHandler}>
          Create Game
        </button>
      </div>
    )
  }
})

module.exports = Create
