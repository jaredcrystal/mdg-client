import React from 'react'
import axios from 'axios'
import WebSocket from 'ws'
import _ from 'lodash'
import {server, WSServer} from '../../../config.js'

import Chat from 'appCommon/chat'

import styles from './queue.css'

const Queue = React.createClass({

  ///////////////////////////
  //  COMPONENT LIFECYCLE  //
  ///////////////////////////

  getInitialState() {
    return {
      game: this.props.game,
      ws: new WebSocket(WSServer)
    }
  },

  sendUpdateTrigger() {
    // ws.send to indicate that the state has changed
    // and everyone needs the new state for this game
    this.state.ws.send(this.props.game.gameName)
  },

  componentDidMount() {
    const self = this
    this.state.ws.on('open', function open() {
      self.sendUpdateTrigger()
    });

    // register as listener for new game state
    this.state.ws.on('message', function(data, flags) {
      const game = JSON.parse(data)

      // if not my game, disregard it
      if (game.gameName !== self.state.game.gameName) {
        return
      }

      // if the game creator left, kick everyone out
      if (!game.creator) {
        self.state.ws.close()
        self.props.leaveQueue()
        return
      }

      if (game.gameState === 'Selection') {
        self.state.ws.close()
        self.props.goToSelection()
        return
      }

      // since it's my game, update state
      self.setState({
        game: JSON.parse(data)
      })
    });
  },

  /////////////////
  //  API CALLS  //
  /////////////////

  leave() {
    const self = this
    axios.delete(server + '/game/custom/player', {
      params: {
        gameName: this.props.game.gameName,
        player: this.props.username
      }
    })
    .then(function (response) {
      // let everyone else know that I left...
      self.sendUpdateTrigger()
      // close the websocket on my way out...
      self.state.ws.close()
      // and then leave
      self.props.leaveQueue()
    })
    .catch(function (error) {
      console.log("leave game error", error)
    })
  },

  sendMessage(message) {
    const self = this
    axios.post(server + '/game/custom/queuechat', {
      gameName: this.props.game.gameName,
      player: this.props.username,
      message: message
    })
    .then(function (response) {
      // let everyone else know that I sent a message
      self.sendUpdateTrigger()
    })
    .catch(function (error) {
      console.log("sending queue chat error", error)
    })
  },

  joinTeam(teamNum) {
    const self = this
    axios.post(server + '/game/custom/team', {
      gameName: this.props.game.gameName,
      player: this.props.username,
      teamNum: teamNum
    })
    .then(function (response) {
      // let everyone else know that I moved...
      self.sendUpdateTrigger()
    })
    .catch(function (error) {
      console.log("move team error", error)
    })
  },

  startGame() {
    const self = this
    axios.post(server + '/game/custom/state', {
      gameName: this.props.game.gameName,
      gameState: 'Selection'
    })
    .then(function (response) {
      // let everyone else know that we're in selection gameState...
      self.sendUpdateTrigger()
    })
    .catch(function (error) {
      console.log("start game error", error)
    })
  },

  ///////////////
  //  HELPERS  //
  ///////////////

  isMyTeam(teamNum) {
    return this.state.game.teams[teamNum].includes(this.props.username)
  },

  isCreator() {
    return this.state.game.creator == this.props.username
  },

  /////////////////
  //  RENDERING  //
  /////////////////

  renderBlanks(num, teamNum) {
    if (this.isMyTeam(teamNum)) {
      num += 1
    }
    var result = []
    for (var i=0; i < num; i ++ ) {
      result.push(<tr key={i}><td className={styles.tdEmpty}></td></tr>)
    }
    return result
  },

  renderUndecided() {
    return this.state.game.teams[0].map((username) => {
      return (
        <tr key={username}>
          <td className={styles.td}>{username}</td>
        </tr>
      )
    })
  },

  renderJoinTeam(numFrees, teamNum) {
    // if more people can be added and I'm not already in that team
    if (numFrees > 0 && !this.isMyTeam(teamNum)) {
      return (
        <tr>
          <td className={styles.td}>
            <button onClick={() => this.joinTeam(teamNum)} className={styles.joinTeamBtn}>+</button>
          </td>
        </tr>
      )
    }
  },

  renderTeam(team) {
    return team.map((member) => {
      return (<tr key={member}><td className={styles.td}>{member}</td></tr>)
    })
  },

  renderTeams() {
    const game = this.state.game
    return _.keys(game.teams).map((teamNum) => {
      if (teamNum === '0') { return }
      const team = game.teams[teamNum]
      return (
        <table key={teamNum} className={styles.table}>
          <tbody>
            <tr><th className={styles.th}>Team #{teamNum}</th></tr>
            {this.renderTeam(team)}
            {this.renderJoinTeam(game.teamSize - team.length, teamNum)}
            {this.renderBlanks(game.teamSize - team.length - 1, teamNum)}
          </tbody>
        </table>
      )
    })
  },

  renderTeamsList() {
    if (this.state.game.teamSize === 1) {
      return
    }
    return (
      <div className={styles.teamsWrapper}>
        {this.renderTeams()}
      </div>
    )
  },

  renderStartBtn() {
    const game = this.state.game
    const teams = game.teams
    let numPlayersOnTeams = 0
    _.keys(teams).forEach((key) => {
        if (key !== '0') {
          numPlayersOnTeams += teams[key].length
        }
    })
    const numPlayersNeeded = game.numTeams * game.teamSize
    const enabled = (numPlayersNeeded === numPlayersOnTeams) ||
                    (game.teamSize === 1 && game.teams[0].length === numPlayersNeeded)

    if (!this.isCreator()) {
      const text = enabled ? 'Waiting for game creator to start' : 'Waiting for more players'
      return (<p className={styles.rightText}>{text}</p>)
    }

    const style = enabled ? styles.startBtn : styles.disabledBtn
    const text = game.teamSize === 1 && !enabled ? game.teams[0].length + '/' + numPlayersNeeded : 'Start Game'
    return (
      <button disabled={!enabled} className={style} onClick={this.startGame}>{text}</button>
    )
  },

  render() {
    const game = this.state.game
    const undecidedStyle = game.teamSize === 1 ? styles.singleTable : styles.table
    return (
      <div className={styles.blackBox}>
        <p className={styles.text}>Creator: {game.creator}</p>
        <p className={styles.text}>Name: {game.gameName}</p>
        <p className={styles.text}>Format: {game.numTeams} teams of {game.teamSize}</p>
        <div className={styles.bigWrapper}>
          <div className={styles.teamsWrapper}>
            <table className={undecidedStyle}>
              <tbody>
                {this.renderUndecided()}
                {this.renderJoinTeam(game.numTeams * game.teamSize - game.teams[0].length, 0)}
                {this.renderBlanks(game.numTeams * game.teamSize - game.teams[0].length - 1, 0)}
              </tbody>
            </table>
          </div>
          {this.renderTeamsList()}
        </div>
        <Chat sendMessageHandler={this.sendMessage} messages={this.state.game.queueChat} />
        <button className={styles.leaveBtn} onClick={this.leave}>Leave Queue</button>
        {this.renderStartBtn()}
      </div>
    )
  }
})

module.exports = Queue
