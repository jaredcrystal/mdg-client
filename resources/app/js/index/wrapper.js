import React from 'react'
import Play from './wrapper/play.js'
import Queue from './wrapper/queue.js'
import Selection from './wrapper/selection.js'
import Learn from './wrapper/learn.js'

import styles from './wrapper.css'

// This file to handle routing via state
const Wrapper = React.createClass({

  getInitialState() {
    return {
      page: 'Play',
      playPage: 'Play',
      game: {}
    }
  },

  goToPage(page) {
    // don't let people leave the selection page except to return to play after game
    if (page !== 'Play' && this.state.page === 'Selection') {
      return
    }
    const playPages = ['Play', 'Queue', 'Selection']
    const nextPage = playPages.includes(page) ? page : this.state.playPage
    this.setState({
      page: page,
      playPage: nextPage
    })
  },

  enterQueue(game) {
    this.setState({
      page: 'Queue',
      playPage: 'Queue',
      game: game
    })
  },

  leaveQueue() {
    this.setState({
      page: 'Play',
      playPage: 'Queue',
      game: {}
    })
  },

  attemptLogout() {
    // don't let people logout during selection
    if (this.state.page !== 'Selection') {
      this.props.logout()
    }
  },

  renderNavLinks() {
    const pages = [this.state.playPage, 'Learn']
    const notSelectedStyle = this.state.playPage === 'Selection' ? styles.disabledPage : styles.notSelectedPage
    return pages.map((page) => {
      const style = this.state.page === page ? styles.selectedPage : notSelectedStyle
      return (
        <li key={page} className={style} onClick={() => this.goToPage(page)}>
          {page}
        </li>
      )
    })
  },

  renderPage() {
    return {
      'Play': <Play
                username={this.props.username}
                enterQueue={this.enterQueue} />,
      'Queue': <Queue
                  username={this.props.username}
                  leaveQueue={this.leaveQueue}
                  game={this.state.game}
                  goToSelection={() => this.goToPage('Selection')} />,
      'Selection': <Selection
                      username={this.props.username}
                      game={this.state.game}
                      goToPlay={() => this.goToPage('Play')} />,
      'Learn': <Learn />
    }[this.state.page]
  },

  render() {
    const notSelectedStyle = this.state.playPage === 'Selection' ? styles.disabledPage : styles.notSelectedPage
    return (
      <div>
        <ul>
          {this.renderNavLinks()}
          <li onClick={this.attemptLogout} className={notSelectedStyle}>Logout</li>
        </ul>
        {this.renderPage()}
      </div>
    )
  }
})

module.exports = Wrapper
