'use strict';
var packager = require('electron-packager');
var options = {
    'arch': 'ia32',
    'platform': 'win32',
    'dir': '.',
    // 'app-copyright': 'Jared Crystal',
    'app-version': '0.0.1',
    // 'asar': true,
    // 'icon': './app.ico',
    'name': 'MDG',
    'out': '.',
    'overwrite': true,
    'prune': true,
    'version': '1.4.6',
    'win32metadata': {
        'CompanyName': 'MDG',
        'FileDescription': 'MDG', /*This is what display windows on task manager, shortcut and process*/
        'OriginalFilename': 'MDG',
        'ProductName': 'MDG',
        'InternalName': 'MDG'
    }
};
packager(options, function done_callback(err, appPaths) {
    console.log("Error: ", err);
    console.log("appPaths: ", appPaths);
});

// electron-packager . MDG --platform=win32 --arch=ia32 --version=1.4.6
